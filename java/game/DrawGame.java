package game;

import java.awt.Color;
import java.awt.Graphics;

/**
 * DrawGame has some static methods which takes a Graphic object and draws
 * different figures on it. This class is used by the GamePanel.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class DrawGame {

    private static double base = 1.005;
    private static double bottom;
    private static int closeLeft, closeRight, farLeft, farRight;
    private static int closeWidth;
    private static double compressHeight;
    private static double drawDistance;
    private static Color shadows = Color.DARK_GRAY;
    private static double top;

    /**
     * Draws the floor on the Graphics object. The floor is defined in the
     * Pillar class.
     * 
     * @param gamePanel
     *            the panel the graphics object will be painted on
     * @param g
     *            the graphics object the method will paint on
     * @param height
     *            the height the graphics object will paint
     */
    public static void drawFloor(GamePanel gamePanel, Graphics g, double height) {
        setHeight(height);
        closeWidth = gamePanel.getWidth()
                / gamePanel.getGame().getBoard().getNumberOfPillars();
        bottom = gamePanel.getHeight();
        top = (int) (gamePanel.getHeight() * compressHeight);

        for (int i = 0; i < gamePanel.getGame().getBoard().getNumberOfPillars(); i++) {
            closeLeft = i * closeWidth;
            closeRight = closeLeft + closeWidth;
            farLeft = gamePanel.getWidth() / 2;
            farRight = farLeft;

            for (int j = 0; j < gamePanel.getGame().getBoard().getPillar(i)
                    .getNumberOfIntervals(); j++) {

                double y[] = getYCoordinate(gamePanel, i, j);

                if (y[0] == -1 || y[1] == -1) {
                    break;
                }
                int[] xPointsTemp = {
                        (int) (getXCoordinate(gamePanel, closeLeft, bottom,
                                farLeft, top, y[0])),
                        (int) (getXCoordinate(gamePanel, closeLeft, bottom,
                                farLeft, top, y[1])),
                        (int) (1 + getXCoordinate(gamePanel, closeRight,
                                bottom, farRight, top, y[1])),
                        (int) (1 + getXCoordinate(gamePanel, closeRight,
                                bottom, farRight, top, y[0])) };

                int[] yPointsTemp = { (int) y[0], (int) y[1], (int) y[1],
                        (int) y[0] };
                g.setColor(gamePanel.getGame().getBoard().getPillar(i)
                        .getInterval(j).getColor());
                g.fillPolygon(xPointsTemp, yPointsTemp, 4);
            }
        }
    }

    /**
     * Draws the shadow of the floor on the Graphics object.
     * 
     * @param gamePanel
     *            the panel the graphics object will be painted on
     * @param g
     *            the graphics object the method will paint on
     * @param height
     *            the height the graphics object will paint
     */
    public static void drawShadows(GamePanel gamePanel, Graphics g,
            double height) {
        setHeight(height);
        g.setColor(shadows);
        closeWidth = gamePanel.getWidth()
                / gamePanel.getGame().getBoard().getNumberOfPillars();
        bottom = gamePanel.getHeight();
        top = (int) (gamePanel.getHeight() * compressHeight);

        for (int i = 0; i < gamePanel.getGame().getBoard().getNumberOfPillars(); i++) {
            closeLeft = i * closeWidth;
            closeRight = closeLeft + closeWidth;
            farLeft = gamePanel.getWidth() / 2;
            farRight = farLeft;

            for (int j = 0; j < gamePanel.getGame().getBoard().getPillar(i)
                    .getNumberOfIntervals(); j++) {

                double y[] = getYCoordinate(gamePanel, i, j);

                if (y[0] == -1 || y[1] == -1) {
                    break;
                }
                int shadowHeigthFront = (int) (0.5 + (getXCoordinate(gamePanel,
                        closeRight, bottom, farRight, top, y[0]) - getXCoordinate(
                        gamePanel, closeLeft, bottom, farLeft, top, y[0])) / 6);
                int shadowHeigthBack = (int) (0.5 + (getXCoordinate(gamePanel,
                        closeRight, bottom, farRight, top, y[1]) - getXCoordinate(
                        gamePanel, closeLeft, bottom, farLeft, top, y[1])) / 6);

                if (getXCoordinate(gamePanel, closeLeft, bottom, farLeft, top,
                        y[1]) < getXCoordinate(gamePanel, closeLeft, bottom,
                        farLeft, top, y[0])) {
                    int[] xPoints = {
                            (int) (getXCoordinate(gamePanel, closeLeft, bottom,
                                    farLeft, top, y[0])),
                            (int) (getXCoordinate(gamePanel, closeLeft, bottom,
                                    farLeft, top, y[0])),
                            (int) (1 + getXCoordinate(gamePanel, closeLeft,
                                    bottom, farLeft, top, y[1])),
                            (int) (1 + getXCoordinate(gamePanel, closeLeft,
                                    bottom, farLeft, top, y[1])) };
                    int[] yPoints = { (int) (y[0] + 0.5),
                            (int) (y[0] + shadowHeigthFront + 0.5),
                            (int) (y[1] + shadowHeigthBack + 0.5),
                            (int) (y[1] + 0.5) };
                    g.fillPolygon(xPoints, yPoints, 4);
                } else if (getXCoordinate(gamePanel, closeLeft, bottom,
                        farLeft, top, y[1]) > getXCoordinate(gamePanel,
                        closeLeft, bottom, farLeft, top, y[0])) {
                    int[] xPoints = {
                            (int) (getXCoordinate(gamePanel, closeRight,
                                    bottom, farRight, top, y[0])),
                            (int) (getXCoordinate(gamePanel, closeRight,
                                    bottom, farRight, top, y[0])),
                            (int) (1 + getXCoordinate(gamePanel, closeRight,
                                    bottom, farRight, top, y[1])),
                            (int) (1 + getXCoordinate(gamePanel, closeRight,
                                    bottom, farRight, top, y[1])) };
                    int[] yPoints = { (int) (y[0] + 0.5),
                            (int) (y[0] + shadowHeigthFront + 0.5),
                            (int) (y[1] + shadowHeigthBack + 0.5),
                            (int) (y[1] + 0.5) };
                    g.fillPolygon(xPoints, yPoints, 4);
                }
                g.fillRect(
                        (int) (0.5 + getXCoordinate(gamePanel, closeLeft,
                                bottom, farLeft, top, y[0])),
                        (int) (0.5 + y[0]),
                        (int) (1 + getXCoordinate(gamePanel, closeRight,
                                bottom, farRight, top, y[0]) - getXCoordinate(
                                gamePanel, closeLeft, bottom, farLeft, top,
                                y[0])), shadowHeigthFront);
            }
        }
    }

    /**
     * This method is used whenever you have to calculate the coordinate in the
     * x-direction of the screen based on the specified parameters. The
     * calculation is based on the equation: a(x1-x0) = (y1-y0)
     * 
     * @param gamePanel
     *            the GamePanel the calculation is based on
     * @param x0
     *            the x-coordinate for the first point in the coordinate system
     * @param y0
     *            the y-coordinate for the first point in the coordinate system
     * @param x1
     *            the x-coordinate for the second point in the coordinate system
     * @param y1
     *            the y-coordinate for the second point in the coordinate system
     * @param intervalEndpoint
     *            the y-coordinate for which the method is to find the
     *            corresponding x-coordinate
     * @return returns the x-coordinate for the point where the line between the
     *         two given points and the given horizontal line intersects
     */
    private static double getXCoordinate(GamePanel gamePanel, double x0,
            double y0, double x1, double y1, double intervalEndpoint) {
        double deltaY = y1 - y0;
        double deltaX = x1 - x0;

        if (deltaX == 0) {
            return gamePanel.getWidth() / 2;
        }

        double a = deltaY / deltaX;
        return ((intervalEndpoint - y0) / a + x0);
    }

    /**
     * This method is used whenever you have to calculate the coordinate in the
     * y-direction on the screen based on the specified pillar and interval.
     * 
     * @param gamePanel
     *            the panel the calculation is based on
     * @param pillar
     *            the pillar the calculation is based on
     * @param interval
     *            the interval the calculation is based on
     * @return returns the y-coordinates on the screen for the given interval on
     *         the given pillar, and these values determines where on the screen
     *         this interval is to be painted.
     */
    private static double[] getYCoordinate(GamePanel gamePanel, int pillar,
            int interval) {
        int start = gamePanel.getGame().getBoard().getPillar(pillar)
                .getInterval(interval).getStart();
        int end = gamePanel.getGame().getBoard().getPillar(pillar)
                .getInterval(interval).getEnd();
        int position = gamePanel.getPlayer().getPosition();

        // If position is after start, start sets to position
        if (position > start) {
            start = position;
        }
        // If end (and start) has past the position, then not draw.
        else if (position > end) {
            return new double[] { -1 };
        }

        // top = distance from top of the screen to the start of draw-area
        // bottom - top = the draw-width in y direction
        // x = distance after the position

        // Final equation (the y-placement in screen for a distance x)
        // y = top + (bottom - top)/x

        int x1 = start - position;
        int y1 = 0;

        // As long as distance is more than 1
        if (x1 > 0) {
            y1 = (int) (top + (bottom - top) * (Math.pow(base, -x1)));
        } else {
            y1 = (int) (top + (bottom - top));
        }

        // Do the same for end-coordinate
        int x2 = end - position;
        int y2 = 0;

        if (x2 > 0) {
            y2 = (int) (top + (bottom - top) * (Math.pow(base, -x2)));
        } else {
            y2 = (int) (top + (bottom - top));
        }

        if (y1 < bottom * drawDistance) {
            return new double[] { -1 };
        }
        if (y2 < bottom * drawDistance) {
            y2 = (int) (bottom * drawDistance);
        }
        return new double[] { y1, y2 };
    }

    /**
     * This method sets the drawHeight for drawing on the graphics objects.
     * 
     * @param height
     *            define the height to draw
     */
    private static void setHeight(double height) {
        compressHeight = 1 - height;
        drawDistance = compressHeight + 0.01 * (1 - compressHeight);
    }
}