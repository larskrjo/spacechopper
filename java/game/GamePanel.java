package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.Timer;

import listener.GamePanelListener;
import listener.TimerListener;
import logic.Game;
import user.Player;
import display.MainFrame;

/**
 * GamePanel is where the playing of the game happens. GamePanel extends
 * JComponent because the drawing of the bike, floor, shadows etc. will be done
 * here.
 * 
 * When a GamePanel is initialized, it creates a Timer which will trigger the
 * repaint method of the GamePanel every 15 ms. GamePanel initializes a
 * GamePanelListener which is used to listen for keyinput.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class GamePanel extends JComponent {

    private static final long serialVersionUID = 1L;
    private Image backgroundImage;
    private final int chopperWidth = 40;
    private boolean decreaseAngel = false;
    private boolean decreaseSpeed = false;
    private Font speedFont = new Font("Times New Roman", Font.BOLD, 50);
    private Game game;
    private GamePanelListener gamePanelListener;
    private boolean handling = true;
    private double height = 0.6;
    private boolean increaseAngel = false;
    private boolean increaseSpeed = false;
    private boolean leftBlueInterval = false;
    private final int lightSourceHeight = 80;
    private final int lightSourceWidth = 100;
    private boolean lost = false;
    private MainFrame mainFrame;
    private int movement = 10;
    private boolean movingLeft = false;
    private boolean movingRight = false;
    private boolean paintShadows = true;
    private Player player;
    private int screenHeight;
    private int screenWidth;
    private Color shadow = new Color(0.4f, 0.4f, 0.4f, 0.5f);
    private double speed = 0;
    private Timer timer;
    private TimerListener timerListener;
    private boolean won = false;

    public GamePanel(MainFrame mainFrame) {
        screenHeight = mainFrame.getHeight();
        screenWidth = mainFrame.getWidth();
        player = new Player(screenWidth / 2, 0, mainFrame);
        requestFocus(true);
        this.mainFrame = mainFrame;
        game = new Game(mainFrame);
        backgroundImage = new ImageIcon(GamePanel.class.getResource("space"
                + game.getBoard().getBoardNumber() + ".jpg")).getImage();
        gamePanelListener = new GamePanelListener(this);
        addKeyListener(gamePanelListener);
        timerListener = new TimerListener(this);
        timer = new Timer(15, timerListener);
        timer.start();
    }

    public double getDrawHeight() {
        return height;
    }

    public Game getGame() {
        return game;
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public int getMovement() {
        return movement;
    }

    public boolean getMovingLeft() {
        return movingLeft;
    }

    public boolean getMovingRight() {
        return movingRight;
    }

    public Player getPlayer() {
        return player;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public double getSpeed() {
        return speed;
    }

    public Timer getTimer() {
        return timer;
    }

    /**
     * The method isDead checks whether you are dead or not. It calculates this
     * from the players x-coordinate. Here, the y-coordinate is irrelevant.
     * 
     * @param dx
     *            the players horizontal position on the screen
     * @return returns true if you are dead, false if not
     */
    private boolean isDead(int dx) {
        if (player.isJumping()) {
            return false;
        }
        dx += 30;
        int roadDelay = 40;
        int pillar = 0;
        int interval = 0;
        for (int i = 0; i < game.getBoard().getNumberOfPillars(); i++) {
            int closeWidth = getWidth() / game.getBoard().getNumberOfPillars();
            if (player.getPositionX() + dx < closeWidth * i + closeWidth) {
                pillar = i;
                break;
            }
        }
        if (game.getBoard().getPillar(pillar).getNumberOfIntervals() == 0
                && !won) {
            lost = true;
            return true;
        }
        for (int i = 0; i < game.getBoard().getPillar(pillar)
                .getNumberOfIntervals(); i++) {
            if (player.getPosition() + roadDelay >= game.getBoard()
                    .getPillar(pillar).getInterval(i).getStart()) {
                if (player.getPosition() + roadDelay >= game.getBoard()
                        .getPillar(pillar).getInterval(i).getEnd()) {
                    continue;
                }
                interval = i;
                break;
            }
        }
        /**
         * If the color of the interval is RED, you loose
         */
        if (game.getBoard().getPillar(pillar).getInterval(interval).getColor() == Color.RED
                && !won) {
            lost = true;
            return true;
        }
        /**
         * If the color of the interval is BLUE, you increase speed
         */
        if (game.getBoard().getPillar(pillar).getInterval(interval).getColor() == Color.BLUE) {
            increaseSpeed = true;
            leftBlueInterval = true;
        } else if (leftBlueInterval) {
            leftBlueInterval = false;
            increaseSpeed = false;
        }
        /**
         * If the color of the interval is CYAN, you have completed the board
         */
        if (game.getBoard().getPillar(pillar).getInterval(interval).getColor() == Color.CYAN
                && !lost) {
            won = true;
            return true;
        }
        if (game.getBoard().getPillar(pillar).getInterval(interval).getStart() <= player
                .getPosition() + roadDelay
                && game.getBoard().getPillar(pillar).getInterval(interval)
                        .getEnd() >= player.getPosition() + roadDelay) {
            return false;
        }
        if (!won) {
            lost = true;
        }
        return true;
    }

    public boolean isDecreaseAngel() {
        return decreaseAngel;
    }

    public boolean isDecreaseSpeed() {
        return decreaseSpeed;
    }

    public boolean isHandling() {
        return handling;
    }

    public boolean isIncreaseAngel() {
        return increaseAngel;
    }

    public boolean isIncreaseSpeed() {
        return increaseSpeed;
    }

    public boolean isLost() {
        return lost;
    }

    public boolean isWon() {
        return won;
    }

    /**
     * This method overrides the paint method for the GamePanels superclass
     * JComponent, and paints the GamePanel.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(backgroundImage, 0, 0, mainFrame.getGamePanel().getWidth(),
                mainFrame.getGamePanel().getHeight(), null);
        DrawGame.drawShadows(this, g, height);
        DrawGame.drawFloor(this, g, height);
        // Shadow for the player:
        int shadowWidth = chopperWidth
                - (int) (3 * player.getPositionY()
                        * (lightSourceWidth - chopperWidth) / (lightSourceHeight - player
                        .getPositionY()));
        g.setColor(shadow);
        if (paintShadows) {
            g.fillOval(player.getPositionX() + 75 - shadowWidth, screenHeight
                    - 30 - (int) player.getPositionY() - 2 * shadowWidth,
                    2 * shadowWidth, 4 * shadowWidth);
        }
        // Draws the player:
        g.drawImage(getPlayer().getChopper().getBike(), player.getPositionX(),
                screenHeight - (int) player.getPositionY() * 5 - 200, null);

        if (isDead(chopperWidth) && !player.isJumping()) {
            game.setGaming(false);
        }
        g.setFont(speedFont);
        g.setColor(Color.RED);
        if (isHandling()) {
            g.drawString(
                    "Time played: "
                            + (new Double(
                                    (int) ((System.currentTimeMillis() - game
                                            .getGameStarted()) / 100))) / 10
                            + "sec", 0, getHeight() / 2);
            g.drawString("Your speed: " + ((int) (speed * 100)) / 100 + "km/h",
                    0, getHeight() / 2 + 40);
        } else if (!isHandling() && isWon()) {
            g.drawString(
                    "Finished at: "
                            + (game.getGameEnded() - game.getGameStarted())
                            / 1000 + "sec", 0, getHeight() / 2);
        } else if (!isHandling() && isLost()) {
            g.drawString(
                    "Died after: "
                            + (game.getGameEnded() - game.getGameStarted())
                            / 1000 + "sec", 0, getHeight() / 2);
        }
    }

    public void setDecreaseAngel(boolean decreaseAngel) {
        this.decreaseAngel = decreaseAngel;
    }

    public void setDecreaseSpeed(boolean decreaseSpeed) {
        this.decreaseSpeed = decreaseSpeed;
    }

    public void setDrawHeight(double height) {
        this.height = height;
    }

    public void setHandling(boolean handling) {
        this.handling = handling;
    }

    public void setIncreaseAngel(boolean increaseAngel) {
        this.increaseAngel = increaseAngel;
    }

    public void setIncreaseSpeed(boolean increaseSpeed) {
        this.increaseSpeed = increaseSpeed;
    }

    public void setLost(boolean lost) {
        this.lost = lost;
    }

    public void setMovingLeft(boolean movingLeft) {
        this.movingLeft = movingLeft;
    }

    public void setMovingRight(boolean movingRight) {
        this.movingRight = movingRight;
    }

    public void setPaintShadows(boolean paintShadows) {
        this.paintShadows = paintShadows;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setWon(boolean won) {
        this.won = won;
    }
}