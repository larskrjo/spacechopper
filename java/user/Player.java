package user;

import display.MainFrame;

/**
 * Player is initialized by the class GamePanel. It holds the players position
 * in the game, whether the player is currently in-jump, the players current
 * chopper and more.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class Player {

    private int a = -10;
    private Chopper chopper;
    private boolean jumping = false;
    private int position;
    private int positionX;
    private double positionY;
    private double timeInAir;
    private MainFrame mainFrame;

    public Player(int positionX, int positionY, MainFrame mainFrame) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.mainFrame = mainFrame;
        chopper = mainFrame.getMainMenu().getChopperPanel().getCurrentChopper();
        timeInAir = 0;
    }

    /**
     * The method calcPositionY calculates the height the player should be above
     * the normal height because of the jump.
     */
    public void calcPositionY() {
        timeInAir += 0.1;
        positionY = (chopper.getJumpSpeed() * timeInAir + 0.5 * a
                * Math.pow(timeInAir, 2));
        if (positionY <= 0) {
            mainFrame.getAudioPlayer().playJumpSound();
            timeInAir = 0;
            positionY = 0;
            jumping = false;
        }
    }

    public Chopper getChopper() {
        return chopper;
    }

    public int getPosition() {
        return position;
    }

    public int getPositionX() {
        return positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    public boolean isJumping() {
        return jumping;
    }

    /**
     * The method jump will make the player jump.
     */
    public void jump() {
        if (!jumping) {
            jumping = true;
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }
}
