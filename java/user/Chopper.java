package user;

import java.awt.Image;

import javax.swing.ImageIcon;

/**
 * Chopper is initialized by the class MainFrame. This class holds on specific
 * information about a chopper, like max speed, acceleration, and an image of
 * the bike.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class Chopper {

    private double acceleration;
    private Image bike = new ImageIcon(Chopper.class.getResource("bike.png"))
            .getImage(), bike2;
    private int jumpSpeed = 15;
    private int maxSpeed;
    private String name;

    public Chopper(int maxSpeed, double acceleration, int id, String name) {
        this.name = name;
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        bike2 = new ImageIcon(Chopper.class.getResource("bike" + id + ".png"))
                .getImage();
    }

    public double getAcceleration() {
        return acceleration;
    }

    public Image getBike() {
        return bike;
    }

    public Image getBike2() {
        return bike2;
    }

    public int getJumpSpeed() {
        return jumpSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getName() {
        return name;
    }

    public void setBike(Image bike) {
        this.bike = bike;
    }
}