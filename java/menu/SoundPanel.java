package menu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import display.MainFrame;

/**
 * SoundPanel is intialized by the MainMenu object. SoundPanel have JSliders to
 * smoothly change soundlevel and buttons for muting.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class SoundPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private JButton back = new JButton("               Back                ");
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private MainFrame mainFrame;
    private JCheckBox muteGame = new JCheckBox("Mute", false);
    private JCheckBox muteMenu = new JCheckBox("Mute", false);
    private JPanel panel = new JPanel();

    public SoundPanel(final MainFrame mainFrame) {

        this.mainFrame = mainFrame;

        back.setBackground(Color.RED);
        back.setFocusable(false);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "menuPanel");
                mainFrame.getMainMenu().getMenuPanel().requestFocus();
            }
        });
        muteGame.setFocusable(false);
        muteGame.setBackground(Color.RED);
        muteGame.setBorder(BorderFactory.createEmptyBorder());
        muteGame.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent arg0) {
                if(mainFrame.getAudioPlayer().isMuteGame()){
                    mainFrame.getAudioPlayer().setMuteGame(false);
                } else {
                    mainFrame.getAudioPlayer().setMuteGame(true);
                    mainFrame.getAudioPlayer().stopGameSound();
                }
            }
        });
        muteMenu.setFocusable(false);
        muteMenu.setBackground(Color.RED);
        muteMenu.setBorder(BorderFactory.createEmptyBorder());
        muteMenu.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent arg0) {
                if(mainFrame.getAudioPlayer().isMuteMenu()){
                    mainFrame.getAudioPlayer().setMuteMenu(false);
                    mainFrame.getAudioPlayer().playMenuSound();
                } else {
                    mainFrame.getAudioPlayer().setMuteMenu(true);
                    mainFrame.getAudioPlayer().stopMenuSound();
                }
            }
        });

        JPanel menuPanel = new JPanel();
        menuPanel.setBackground(Color.RED);
        JPanel menuPanelContent = new JPanel();
        menuPanelContent.setLayout(new GridLayout(3, 0));
        menuPanelContent.setBackground(Color.RED);

        menuPanelContent.add(new JLabel("Volume in menu"));
        menuPanelContent.add(muteMenu);

        menuPanel.add(menuPanelContent, BorderLayout.CENTER);

        JPanel gamePanel = new JPanel();
        gamePanel.setBackground(Color.RED);
        JPanel gamePanelContent = new JPanel();
        gamePanelContent.setLayout(new GridLayout(3, 0));
        gamePanelContent.setBackground(Color.RED);

        gamePanelContent.add(new JLabel("Volume in game"));
        gamePanelContent.add(muteGame);

        gamePanel.add(gamePanelContent, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        GridBagConstraints c = new GridBagConstraints();
        panel.setLayout(new GridBagLayout());
        panel.setOpaque(false);

        c.weightx = 1;
        c.weighty = 10;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(new JLabel(), c);
        c.weighty = 1;
        c.gridy = 1;
        panel.add(gamePanel, c);
        c.gridy = 2;
        panel.add(menuPanel, c);
        c.gridy = 3;
        panel.add(back, c);
        c.gridy = 4;
        c.weighty = 10;
        panel.add(new JLabel(), c);

        add(panel, BorderLayout.CENTER);
    }

    /**
     * The method paint overrides the superclass' paint method and paints
     * SoundPanel
     */
    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        paintChildren(g);
    }
}
