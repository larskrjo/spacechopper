package menu;

import java.awt.CardLayout;

import javax.swing.JPanel;

import listener.MainMenuListener;
import display.MainFrame;

/**
 * MainMenu is initialized by the MainFrame object, and this JPanel is
 * invisible. The MainMenu adds MenuPanel, ChopperPanel, BoardPanel, SoundPanel,
 * HighscorePanel, HelpPanel and LoginPanel. MainMenu has CardLayout as its
 * layout and is therfore able to switch between the panels.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class MainMenu extends JPanel {

    private static final long serialVersionUID = 1L;
    private BoardPanel boardPanel;
    private ChopperPanel chopperPanel;
    private HelpPanel helpPanel;
    private HighscorePanel highscorePanel;
    private LoginPanel loginPanel;
    private MenuPanel menuPanel;
    private SoundPanel soundPanel;

    public MainMenu(MainFrame mainFrame) {
        addKeyListener(new MainMenuListener(mainFrame));
        if (mainFrame.isOnlineMode()) {
            loginPanel = new LoginPanel(mainFrame);
            loginPanel.setFocusable(true);
        }
        chopperPanel = new ChopperPanel(mainFrame);
        chopperPanel.setFocusable(true);
        boardPanel = new BoardPanel(mainFrame);
        boardPanel.setFocusable(true);
        menuPanel = new MenuPanel(mainFrame, chopperPanel, boardPanel);
        menuPanel.setFocusable(true);
        soundPanel = new SoundPanel(mainFrame);
        soundPanel.setFocusable(true);
        if (mainFrame.isOnlineMode()) {
            highscorePanel = new HighscorePanel(mainFrame, boardPanel,
                    menuPanel);
            highscorePanel.setFocusable(true);
        }
        helpPanel = new HelpPanel(mainFrame);
        helpPanel.setFocusable(true);

        setLayout(new CardLayout());

        add(menuPanel, "menuPanel");
        add(chopperPanel, "chopperPanel");
        add(boardPanel, "boardPanel");
        add(soundPanel, "soundPanel");
        if (mainFrame.isOnlineMode()) {
            add(highscorePanel, "highscorePanel");
            add(loginPanel, "loginPanel");
        }
        add(helpPanel, "helpPanel");

        if (mainFrame.isOnlineMode()) {
            ((CardLayout) getLayout()).show(this, "loginPanel");
        } else {
            ((CardLayout) getLayout()).show(this, "menuPanel");
        }
    }

    public BoardPanel getBoardPanel() {
        return boardPanel;
    }

    public ChopperPanel getChopperPanel() {
        return chopperPanel;
    }

    public HelpPanel getHelpPanel() {
        return helpPanel;
    }

    public HighscorePanel getHighscorePanel() {
        return highscorePanel;
    }

    public MenuPanel getMenuPanel() {
        return menuPanel;
    }

    public SoundPanel getSoundPanel() {
        return soundPanel;
    }
}
