package menu;

import game.GamePanel;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import display.MainFrame;

/**
 * MenuPanel is initialized by the MainMenu object. MenuPanel extends JPanel
 * because it holds JButtons, JLabels etc. The JButtons all have their own
 * ActionListener for listening on the button.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class MenuPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private JLabel boardImage = new JLabel();
    private GridBagConstraints c = new GridBagConstraints();
    private JButton chopper = new JButton("   Chopper settings   ");
    private JLabel chopperImage = new JLabel();
    private JLabel chopperTitle;
    private JButton exit = new JButton("               Exit                ");
    private JButton help = new JButton("               Help               ");
    private JButton highscore = new JButton("          Highscore          ");
    private MainFrame mainFrame;
    private JLabel name = new JLabel();
    private JButton newGame = new JButton("         New Game         ");
    private JPanel panel = new JPanel();
    private JButton sound = new JButton("     Sound settings      ");
    private JButton track = new JButton("      Track settings      ");
    private JLabel trackTitle;

    public MenuPanel(final MainFrame mainFrame, ChopperPanel chopperPanel,
            final BoardPanel boardPanel) {

        this.mainFrame = mainFrame;

        name.setBackground(new Color(0.5f, 0.2f, 0.2f));
        newGame.setBackground(Color.RED);
        newGame.setFocusable(false);
        chopper.setBackground(Color.RED);
        chopper.setFocusable(false);
        track.setBackground(Color.RED);
        track.setFocusable(false);
        sound.setBackground(Color.RED);
        sound.setFocusable(false);
        highscore.setBackground(Color.RED);
        highscore.setFocusable(false);
        help.setBackground(Color.RED);
        help.setFocusable(false);
        exit.setBackground(Color.RED);
        exit.setFocusable(false);

        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (mainFrame.getGamePanel() != null) {
                    mainFrame.getContentPane().remove(mainFrame.getGamePanel());
                }
                GamePanel gamePanel = new GamePanel(mainFrame);
                gamePanel.setFocusable(true);
                gamePanel.requestFocus();
                mainFrame.setGamePanel(gamePanel);
                mainFrame.getContentPane().add(gamePanel, "game");
                ((CardLayout) mainFrame.getContentPane().getLayout())
                        .next(mainFrame.getContentPane());
                mainFrame.getGamePanel().requestFocus();

                // mainFrame.getAudioPlayer().stop();
                // mainFrame.getAudioPlayer().setCurrentSong(1);
                // mainFrame.getAudioPlayer().getGameClip()
                // .setMicrosecondPosition(0);
                // mainFrame.getAudioPlayer().play();
                mainFrame.getAudioPlayer().stopMenuSound();
                mainFrame.getAudioPlayer().playGameSound();
            }
        });
        chopper.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "chopperPanel");
                mainFrame.getMainMenu().getChopperPanel().requestFocus();
            }
        });

        track.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                boardPanel.refreshBoards();
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "boardPanel");
                mainFrame.getMainMenu().getBoardPanel().requestFocus();
            }
        });

        sound.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "soundPanel");
                mainFrame.getMainMenu().getSoundPanel().requestFocus();
            }
        });
        highscore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                mainFrame
                        .getMainMenu()
                        .getHighscorePanel()
                        .refreshHighscore(
                                mainFrame.getMainMenu().getHighscorePanel()
                                        .getIndex());
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "highscorePanel");
                mainFrame.getMainMenu().getHighscorePanel().requestFocus();
            }
        });

        help.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "helpPanel");
                mainFrame.getMainMenu().getHelpPanel().requestFocus();
            }
        });

        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });

        setLayout(new BorderLayout());
        add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridBagLayout());
        panel.setOpaque(false);

        c.weightx = 10;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 10;
        Font font = new Font("Times New Roman", 0, 50);
        Color color = new Color(0.1f, 0.1f, 0.9f, 0.8f);
        chopperTitle = new JLabel("Chopper: "
                + mainFrame.getChoppers()
                        .get(chopperPanel.getCurrentChopperIndex()).getName());
        chopperTitle.setForeground(color);
        chopperTitle.setFont(font);
        chopperImage = new JLabel(new ImageIcon(chopperPanel
                .getCurrentChopper().getBike2()));
        panel.add(chopperImage, c);
        c.gridheight = 2;
        panel.add(chopperTitle, c);
        c.weightx = 10;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 1;
        trackTitle = new JLabel("Track: "
                + boardPanel.getBoard(0).getPillar(0).getName()
                        .get(boardPanel.getBoardIndex()));
        trackTitle.setFont(font);
        trackTitle.setForeground(color);
        boardImage = new JLabel(new ImageIcon(boardPanel.getCurrentBoard()
                .getImage()));
        boardImage.setBorder(BorderFactory.createLineBorder(color));
        c.gridheight = 10;
        panel.add(boardImage, c);
        c.gridheight = 2;
        panel.add(trackTitle, c);
        c.gridheight = 1;

        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridLayout(2, 1));
        namePanel.setOpaque(false);

        name.setSize(400, 10);
        name.setFont(new Font("Times New Roman", 0, 30));
        name.setForeground(Color.RED);

        JLabel nameLabel = new JLabel("User:");
        nameLabel.setForeground(Color.RED);
        nameLabel.setVerticalAlignment(SwingConstants.CENTER);
        nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        nameLabel.setFont(new Font("Times New Roman", 0, 25));

        namePanel.add(nameLabel);
        namePanel.add(name);

        c.weightx = 1;
        c.weighty = 10;
        c.gridx = 1;
        c.gridy = 0;
        panel.add(new JLabel(), c);
        c.gridy = 1;
        c.weighty = 1;
        c.gridy = 1;
        if (mainFrame.isOnlineMode()) {
            panel.add(namePanel, c);
            c.gridy = 2;
            panel.add(newGame, c);
            c.gridy = 3;
            panel.add(chopper, c);
            c.gridy = 4;
            panel.add(track, c);
            c.gridy = 5;
            panel.add(sound, c);
            c.gridy = 6;
            panel.add(highscore, c);
            c.gridy = 7;
            panel.add(help, c);
            c.gridy = 8;
            panel.add(exit, c);
            c.gridy = 9;
            c.weighty = 10;
            panel.add(new JLabel(), c);
            c.weighty = 1;
        } else {
            panel.add(newGame, c);
            c.gridy = 2;
            panel.add(chopper, c);
            c.gridy = 3;
            panel.add(track, c);
            c.gridy = 4;
            panel.add(sound, c);
            c.gridy = 5;
            panel.add(help, c);
            c.gridy = 6;
            panel.add(exit, c);
            c.gridy = 7;
            c.weighty = 10;
            panel.add(new JLabel(), c);
            c.weighty = 1;
        }
    }

    public JLabel getBoardImage() {
        return boardImage;
    }

    public JLabel getChopperImage() {
        return chopperImage;
    }

    public JLabel getChopperTitle() {
        return chopperTitle;
    }

    public String getPlayerName() {
        return name.getText();
    }

    public JLabel getTrackTitle() {
        return trackTitle;
    }

    /**
     * The method paint overrides the superclass' paint method and paints
     * MenuPanel
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        paintChildren(g);
    }

    public void setPlayerName(String name) {
        this.name.setText(name);
    }
}
