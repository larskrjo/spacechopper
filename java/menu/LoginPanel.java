package menu;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import display.MainFrame;

/**
 * LoginPanel is initialized by the MainMenu object, and this is the first panel
 * that will be visible if you connects to the database.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class LoginPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private JButton backButton1 = new JButton(
            "                 Back                  ");
    private JButton backButton2 = new JButton(
            "                 Back                  ");
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private GridBagConstraints c = new GridBagConstraints();
    private JButton exitButton = new JButton(
            "                        Exit                     ");
    private Font font = new Font("Times New Roman", 0, 60);
    private JButton loginButton = new JButton(
            "                Submit               ");
    private JPanel loginPanel = new JPanel();
    private JButton loginPanelButton = new JButton(
            "                     Login                     ");
    private MainFrame mainFrame;
    private JPanel mainPanel = new JPanel();
    private JTextField nameLoginField = new JTextField();
    private JTextField nameNewPlayerField = new JTextField();
    private JPanel panel = new JPanel();
    private JPasswordField passwordLoginField = new JPasswordField();
    private JPasswordField passwordNewPlayerField = new JPasswordField();
    private JButton registerButton = new JButton(
            "                Submit               ");
    private JPanel registerPanel = new JPanel();
    private JButton registerPanelButton = new JButton(
            "               New Player                ");
    private JLabel titleLogin;
    private JLabel titleMain;
    private JLabel titleRegister;

    public LoginPanel(final MainFrame mainFrame) {

        this.mainFrame = mainFrame;

        setLayout(new GridBagLayout());
        panel.setLayout(new CardLayout());
        panel.setOpaque(false);
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setOpaque(false);
        loginPanel.setLayout(new GridBagLayout());
        loginPanel.setOpaque(false);
        registerPanel.setLayout(new GridBagLayout());
        registerPanel.setOpaque(false);

        nameLoginField.setFont(new Font("Times New Roman", 0, 30));
        nameLoginField.setColumns(7);
        passwordLoginField.setFont(new Font("Times New Roman", 0, 30));
        passwordLoginField.setColumns(7);
        nameNewPlayerField.setFont(new Font("Times New Roman", 0, 30));
        nameNewPlayerField.setColumns(7);
        passwordNewPlayerField.setFont(new Font("Times New Roman", 0, 30));
        passwordNewPlayerField.setColumns(7);

        // Adding panel(CardLayout) to the LoginPanel(GridBagLayout)
        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 1;
        c.gridy = 0;
        add(new JLabel(""), c);
        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 1;
        c.gridy = 2;
        add(new JLabel(""), c);
        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 1;
        add(new JLabel(""), c);
        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 1;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        add(panel, c);

        // Adding mainPanel(GridBagLayout) to panel(CardLayout)

        titleMain = new JLabel("Welcome to Space Chopper");
        titleMain.setFont(font);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        mainPanel.add(titleMain, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 2;
        mainPanel.add(loginPanelButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 3;
        mainPanel.add(registerPanelButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 4;
        mainPanel.add(exitButton, c);

        panel.add(mainPanel, "mainPanel");

        // ------------------------------------- //

        // Adding loginPanel(GridBagLayout) to panel(CardLayout)

        titleLogin = new JLabel("             Login             ");
        titleLogin.setOpaque(false);
        titleLogin.setBackground(null);
        titleLogin.setFont(font);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        loginPanel.add(titleLogin, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 2;
        loginPanel.add(nameLoginField, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 3;
        loginPanel.add(passwordLoginField, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 4;
        loginPanel.add(loginButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 5;
        loginPanel.add(backButton1, c);

        panel.add(loginPanel, "loginPanel");

        // ------------------------------------- //

        // Adding registerPanel(GridBagLayout) to panel(CardLayout)

        registerPanel.setBackground(Color.RED);

        titleRegister = new JLabel("             Register             ");
        titleRegister.setOpaque(false);
        titleRegister.setBackground(null);
        titleRegister.setFont(font);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        registerPanel.add(titleRegister, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 2;
        registerPanel.add(nameNewPlayerField, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 3;
        registerPanel.add(passwordNewPlayerField, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 4;
        registerPanel.add(registerButton, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 5;
        registerPanel.add(backButton2, c);

        panel.add(registerPanel, "registerPanel");

        // ------------------------------------- //

        loginPanelButton.setFocusable(false);
        loginPanelButton.setBackground(Color.RED);
        loginPanelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) panel.getLayout()).show(panel, "loginPanel");
            }
        });

        registerPanelButton.setFocusable(false);
        registerPanelButton.setBackground(Color.RED);
        registerPanelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) panel.getLayout()).show(panel, "registerPanel");
            }
        });

        backButton1.setFocusable(false);
        backButton1.setBackground(Color.RED);
        backButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) panel.getLayout()).show(panel, "mainPanel");
            }
        });

        backButton2.setFocusable(false);
        backButton2.setBackground(Color.RED);
        backButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) panel.getLayout()).show(panel, "mainPanel");
            }
        });

        registerButton.setFocusable(false);
        registerButton.setBackground(Color.RED);
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Connection con = HighscorePanel.getConnection();
                    String query = "SELECT username FROM user";
                    PreparedStatement statement = con.prepareStatement(query);
                    ResultSet rs = statement.executeQuery();
                    boolean register = true;
                    while (rs.next()) {
                        if (rs.getString(1)
                                .equals(nameNewPlayerField.getText())) {
                            // Checks if the username already exists, and only
                            // register if that does not exists.
                            register = false;
                        }
                    }
                    if (register) {
                        query = "INSERT INTO user (registered, username, password, level) VALUES(now(), ?, ?, 1)";
                        statement = con.prepareStatement(query);
                        statement.setString(1, nameNewPlayerField.getText());
                        statement.setString(2, new String(
                                passwordNewPlayerField.getPassword()));
                        statement.executeUpdate();
                        mainFrame.getMainMenu().getMenuPanel()
                                .setPlayerName(nameNewPlayerField.getText());
                        updatePlayerLevel();
                        ((CardLayout) mainFrame.getMainMenu().getLayout())
                                .show(mainFrame.getMainMenu(), "menuPanel");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        loginButton.setFocusable(false);
        loginButton.setBackground(Color.RED);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Connection con = HighscorePanel.getConnection();
                    String query = "SELECT username, password FROM user";
                    PreparedStatement statement = con.prepareStatement(query);
                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {
                        if (rs.getString(1).equals(nameLoginField.getText())
                                && rs.getString(2).equals(
                                        new String(passwordLoginField
                                                .getPassword()))) {
                            mainFrame.getMainMenu().getMenuPanel()
                                    .setPlayerName(nameLoginField.getText());
                            updatePlayerLevel();
                            ((CardLayout) mainFrame.getMainMenu().getLayout())
                                    .show(mainFrame.getMainMenu(), "menuPanel");
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        exitButton.setFocusable(false);
        exitButton.setBackground(Color.RED);
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });

        ((CardLayout) panel.getLayout()).show(panel, "mainPanel");
    }

    /**
     * Overrides the superclass' paint method and paints LoginPanel
     */
    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        paintChildren(g);
    }

    /**
     * The method updatePlayerLevel updates the current players level, and make
     * sure the player have the right data stored in the game.
     */
    public void updatePlayerLevel() {
        try {
            Connection con = HighscorePanel.getConnection();
            String query = "SELECT level FROM user WHERE username = ?";
            PreparedStatement statement = con.prepareStatement(query);
            if (mainFrame.getMainMenu() != null) {
                statement.setString(1, mainFrame.getMainMenu().getMenuPanel()
                        .getPlayerName());
                ResultSet result = statement.executeQuery();
                while (result.next()) {
                    for (int i = 0; i < result.getInt(1); i++) {
                        mainFrame.getBoards().get(i).setAvailable(true);
                    }
                    for (int i = result.getInt(1); i < 10; i++) {
                        mainFrame.getBoards().get(i).setAvailable(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
