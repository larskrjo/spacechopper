package menu;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import map.Board;
import display.MainFrame;

/**
 * BoardPanel is intialized by the MainMenu object, and shows different boards.
 * It holds on an array of Board objects, which describes the different boards
 * avaliable in the game.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class BoardPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private JButton back = new JButton("               Back                ");
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private JPanel board = new JPanel();
    private Board[] boards = new Board[10];
    private int i = 0;
    private MainFrame mainFrame;

    public BoardPanel(final MainFrame mainFrame) {
        back.setFocusable(false);
        this.mainFrame = mainFrame;
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                updateIndex();
                mainFrame
                        .getMainMenu()
                        .getMenuPanel()
                        .getTrackTitle()
                        .setText(
                                "Track: "
                                        + getBoard(0).getPillar(0).getName()
                                                .get(i));
                mainFrame
                        .getMainMenu()
                        .getMenuPanel()
                        .getBoardImage()
                        .setIcon(
                                new ImageIcon(mainFrame.getMainMenu()
                                        .getBoardPanel().getCurrentBoard()
                                        .getImage()));
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "menuPanel");
                mainFrame.getMainMenu().getMenuPanel().requestFocus();
            }
        });
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JButton left = new JButton("               Left               ");
        JButton right = new JButton("             Right             ");
        left.setFocusable(false);
        right.setFocusable(false);

        board.setOpaque(false);
        board.setLayout(new CardLayout());

        left.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) board.getLayout()).previous(board);
                if (i == 0)
                    i = 9;
                else
                    i = i - 1;
                if (mainFrame.isOnlineMode()) {
                    mainFrame.getMainMenu().getHighscorePanel().setIndex(i);
                }
            }
        });

        right.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) board.getLayout()).next(board);
                if (i == 9)
                    i = 0;
                else
                    i = i + 1;
                if (mainFrame.isOnlineMode()) {
                    mainFrame.getMainMenu().getHighscorePanel().setIndex(i);
                }
            }
        });

        refreshBoards();

        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 2;
        c.gridy = 0;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 3;
        add(board, c);
        c.gridwidth = 1;

        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 2;
        add(left, c);
        left.setBackground(Color.RED);
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 3;
        c.gridy = 2;
        add(right, c);
        right.setBackground(Color.RED);
        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 4;
        c.gridy = 0;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 3;
        add(back, c);
        back.setBackground(Color.RED);

        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 2;
        c.gridy = 4;
        add(new JLabel(""), c);

    }

    /**
     * The method createBoard creates a board panel with an image and a name.
     * 
     * @param board
     *            is used to get the board images
     * @param index
     *            is used to determine which name the board should have
     * @return returns the panel to the gived board
     */
    private JPanel createBoard(Board board, int index) {

        JPanel bikeP1 = new JPanel();

        bikeP1.setLayout(new GridBagLayout());

        bikeP1.setBackground(Color.RED);

        GridBagConstraints cb = new GridBagConstraints();

        JLabel bikeL1 = new JLabel(new ImageIcon(board.getImage()));
        if (getBoard(index).isAvailable()) {
            bikeL1 = new JLabel(new ImageIcon(board.getImage()));
        } else {
            bikeL1 = new JLabel(new ImageIcon(board.getLockedImage()));
        }
        cb.gridy = 1;
        cb.gridx = 0;
        bikeL1.setOpaque(false);
        bikeL1.setBackground(null);
        cb.gridwidth = 2;
        bikeP1.add(bikeL1, cb);

        Font font = new Font("Times New Roman", 0, 50);

        JLabel name = new JLabel("Track: "
                + getBoard(0).getPillar(0).getName().get(index));

        name.setOpaque(false);
        name.setBackground(null);
        name.setFont(font);

        cb.gridy = 0;

        bikeP1.add(name, cb);

        return bikeP1;
    }

    public Board getBoard(int index) {
        return boards[index];
    }

    public int getBoardIndex() {
        return i;
    }

    public Board getCurrentBoard() {
        return boards[i];
    }

    /**
     * Overrides the superclass' paint method to paint the BoardPanel.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        paintChildren(g);
    }

    /**
     * The method refreshBoards refreshes the images and availability of the
     * boards, depending on how many boards you have completed.
     */
    public void refreshBoards() {
        board.removeAll();
        for (int i = 0; i < mainFrame.getBoards().size(); i++) {
            boards[i] = mainFrame.getBoards().get(i);
            board.add(createBoard(mainFrame.getBoards().get(i), i),
                    String.valueOf(i));
        }
        if (!boards[i].isAvailable()) {
            for (int i = 9; i >= 0; i--) {
                if (boards[i].isAvailable()) {
                    ((CardLayout) board.getLayout()).show(board, "" + i);
                    this.i = i;
                    break;
                }
            }
        } else {
            ((CardLayout) board.getLayout()).show(board, "" + i);
        }
    }

    /**
     * The method updateIndex() makes sure you can't play a board that's not
     * available to you.
     */
    private void updateIndex() {
        if (!boards[i].isAvailable()) {
            for (int i = 9; i >= 0; i--) {
                if (boards[i].isAvailable()) {
                    this.i = i;
                    break;
                }
            }
        }
    }
}