package menu;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import user.Chopper;
import display.MainFrame;

/**
 * ChopperPanel is initialized by the MainMenu object, and shows different
 * Choppers. It holds on an array of Choppers, which describes the different
 * choppers avaliable in the game.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class ChopperPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private JButton back = new JButton("               Back                ");
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private JPanel bikePanel;
    private Chopper[] choppers = new Chopper[4];
    private int i = 0;
    private MainFrame mainFrame;

    public ChopperPanel(final MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        back.setFocusable(false);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "menuPanel");
                mainFrame.getMainMenu().getMenuPanel().requestFocus();
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JButton left = new JButton("               Left               "), right = new JButton(
                "             Right             ");
        left.setFocusable(false);
        right.setFocusable(false);
        bikePanel = new JPanel();
        bikePanel.setOpaque(false);
        bikePanel.setLayout(new CardLayout());

        left.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) bikePanel.getLayout()).previous(bikePanel);
                if (i == 0)
                    i = 3;
                else
                    i = i - 1;
            }
        });

        right.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) bikePanel.getLayout()).next(bikePanel);
                if (i == 3)
                    i = 0;
                else
                    i = i + 1;
            }
        });

        for (int i = 0; i < mainFrame.getChoppers().size(); i++) {
            choppers[i] = mainFrame.getChoppers().get(i);
            bikePanel.add(createBike(mainFrame.getChoppers().get(i), i),
                    String.valueOf(i));
        }
        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 2;
        c.gridy = 0;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 3;
        add(bikePanel, c);
        c.gridwidth = 1;

        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 2;
        add(left, c);
        left.setBackground(Color.RED);
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 3;
        c.gridy = 2;
        add(right, c);
        right.setBackground(Color.RED);
        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 4;
        c.gridy = 0;
        add(new JLabel(""), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 3;
        add(back, c);
        back.setBackground(Color.RED);

        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 2;
        c.gridy = 4;
        add(new JLabel(""), c);

    }

    /**
     * The method createBike creates a chopper panel with an image and a name.
     * 
     * @param chopper
     *            is used to get the chopper image and stats
     * @param index
     *            is used to determine which name the chopper should have
     * @return returns the panel to the gived chopper
     */
    public JPanel createBike(Chopper chopper, int index) {

        JPanel bikeP1 = new JPanel();
        bikeP1.setBackground(Color.RED);
        bikeP1.setLayout(new GridBagLayout());
        GridBagConstraints cb = new GridBagConstraints();

        Font font = new Font("Times New Roman", 0, 50);

        JLabel name = new JLabel("Chopper: "
                + mainFrame.getChoppers().get(index).getName());

        name.setOpaque(false);
        name.setBackground(null);
        name.setFont(font);

        cb.gridwidth = 2;
        cb.gridy = 0;

        bikeP1.add(name, cb);
        cb.gridwidth = 1;

        JLabel bikeL1 = new JLabel(new ImageIcon(chopper.getBike2()));
        cb.gridy = 1;
        cb.gridx = 0;
        bikeL1.setOpaque(false);
        bikeL1.setBackground(null);
        cb.gridwidth = 2;
        bikeP1.add(bikeL1, cb);
        cb.gridwidth = 1;
        JLabel speed = new JLabel("Speed"), acceleration = new JLabel(
                "Acceleration");
        speed.setOpaque(false);
        cb.gridy = 2;
        cb.anchor = GridBagConstraints.WEST;
        bikeP1.add(speed, cb);
        cb.gridx = 1;
        cb.anchor = GridBagConstraints.WEST;
        bikeP1.add(acceleration, cb);
        JProgressBar speedPB = new JProgressBar(0, 200);
        speedPB.setValue(chopper.getMaxSpeed());
        JProgressBar accPB = new JProgressBar(90, 97);
        accPB.setValue((int) (chopper.getAcceleration() * 100));
        cb.gridy = 3;
        cb.gridx = 0;
        bikeP1.add(speedPB, cb);
        cb.gridx = 1;
        bikeP1.add(accPB, cb);

        return bikeP1;
    }

    public Chopper getCurrentChopper() {
        return choppers[i];
    }

    public int getCurrentChopperIndex() {
        return i;
    }

    /**
     * Overrides the superclass' paint method to paint the ChopperPanel.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        mainFrame
                .getMainMenu()
                .getMenuPanel()
                .getChopperTitle()
                .setText(
                        "Chopper: "
                                + mainFrame.getChoppers()
                                        .get(getCurrentChopperIndex())
                                        .getName());
        mainFrame
                .getMainMenu()
                .getMenuPanel()
                .getChopperImage()
                .setIcon(
                        new ImageIcon(mainFrame.getMainMenu().getChopperPanel()
                                .getCurrentChopper().getBike2()));
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        paintChildren(g);
    }
}