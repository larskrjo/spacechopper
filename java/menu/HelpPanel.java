package menu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import display.MainFrame;

/**
 * HelpPanel is initialized by the MainMenu object, and shows a JLabel with text
 * which should help you understanding how to play.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class HelpPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private JButton back = new JButton("               Back                ");
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private MainFrame mainFrame;
    private JPanel panel = new JPanel();

    public HelpPanel(final MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        back.setFocusable(false);
        back.setBackground(Color.RED);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "menuPanel");
                mainFrame.getMainMenu().getMenuPanel().requestFocus();
            }
        });

        JPanel helpPanel = new JPanel();
        JLabel help = new JLabel(
        // "<li><Turn with the left and right arrows, accelerate with forward, brake with backward.<br> "
        // +
        // "If you loose a game and you want to try again without changing levels, you can hit F1 to do so quickly.<br>"
        // + "F4 is a shortcut for quitting the game.</body></html>"
                "<html><body><center><h1>Help</h1></center>" + "Keys:" + "<UL>"
                        + "  <LI>\"LEFT ARROW\" Turn left"
                        + "  <LI>\"RIGHT ARROW\" Turn right"
                        + "  <LI>\"UP ARROW\" Accelerate"
                        + "  <LI>\"DOWN ARROW\" Break" + "  <LI>\"SPACE\" Jump"
                        + "  <LI>\"ESC\" Go back to menu"
                        + "  <LI>\"F1\" Restart the previous game"
                        + "  <LI>\"F4\" Quit after a game is played" + "</UL>");

        helpPanel.add(help);
        helpPanel.setBackground(Color.RED);

        setLayout(new BorderLayout());
        GridBagConstraints c = new GridBagConstraints();
        panel.setLayout(new GridBagLayout());
        panel.setOpaque(false);

        c.weightx = 1;
        c.weighty = 10;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(new JLabel(), c);
        c.weighty = 1;
        c.gridy = 1;
        panel.add(helpPanel, c);
        c.gridy = 2;
        panel.add(back, c);
        c.gridy = 3;
        c.weighty = 10;
        panel.add(new JLabel(), c);

        add(panel, BorderLayout.CENTER);
    }

    /**
     * Overrides the superclass' paint method to paint the HelpPanel.
     */
    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        paintChildren(g);
    }
}
