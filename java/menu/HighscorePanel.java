package menu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import display.MainFrame;

/**
 * HighscorePanel is initialized by the MainMenu object, and shows different
 * highscores depending on which board you looking at.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class HighscorePanel extends JPanel {

    private static final long serialVersionUID = 1L;

    /**
     * The method getConnection is a static method and returns the connection to
     * the database.
     * 
     * @return returns the connection to the database
     */
    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
            con = DriverManager.getConnection(
                    "jdbc:mysql://ntnu.progamers.no/prosjekt", "prosjekt",
                    "prosjekt");
        } catch (Exception e) {
            try {
                Class.forName("org.gjt.mm.mysql.Driver").newInstance();
                con = DriverManager.getConnection(
                        "jdbc:mysql://stats.progamers.no/prosjekt", "prosjekt",
                        "prosjekt");
            } catch (Exception e2) {
                return null;
            }
        }
        return con;
    }

    private JButton back = new JButton("               Back                ");
    private Image background = new ImageIcon(
            MenuPanel.class.getResource("background.jpg")).getImage();
    private BoardPanel boardPanel;
    private GridBagConstraints c = new GridBagConstraints();
    private JPanel highscorePanel;
    private JPanel imagePanel;
    private int index = 0;
    private JButton left = new JButton();
    private MainFrame mainFrame;
    private JPanel mainPanel = new JPanel();

    private JButton right = new JButton();

    public HighscorePanel(final MainFrame mainFrame, BoardPanel boardPanel,
            MenuPanel menuPanel) {
        this.mainFrame = mainFrame;
        this.boardPanel = boardPanel;
        index = boardPanel.getBoardIndex();
        back.setFocusable(false);
        back.setBackground(Color.RED);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                        mainFrame.getMainMenu(), "menuPanel");
                mainFrame.getMainMenu().getMenuPanel().requestFocus();
            }
        });

        setLayout(new BorderLayout());
        mainPanel.setLayout(new CardLayout());

        left = new JButton("               Left               ");
        right = new JButton("             Right             ");
        left.setFocusable(false);
        right.setFocusable(false);
        left.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (index == 0)
                    index = 9;
                else
                    index -= 1;
                refreshHighscore(index);
                ((CardLayout) imagePanel.getLayout()).show(imagePanel, ""
                        + index);
                ((CardLayout) highscorePanel.getLayout()).show(highscorePanel,
                        "" + index);
            }
        });

        right.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (index == 9)
                    index = 0;
                else
                    index += 1;
                refreshHighscore(index);
                ((CardLayout) imagePanel.getLayout()).show(imagePanel, ""
                        + index);
                ((CardLayout) highscorePanel.getLayout()).show(highscorePanel,
                        "" + index);
            }
        });

        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setOpaque(false);

        imagePanel = new JPanel();
        imagePanel.setLayout(new CardLayout());

        highscorePanel = new JPanel();
        highscorePanel.setLayout(new CardLayout());

        for (int i = 0; i < 10; i++) {
            imagePanel.add(getImagePanel(i), "" + i);
            refreshHighscore(i);
        }
        c.weightx = 100;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        c.gridheight = 1;
        mainPanel.add(new JLabel(), c);
        c.gridx = 4;
        mainPanel.add(new JLabel(), c);

        c.weightx = 1;
        c.weighty = 100;
        c.gridx = 2;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        mainPanel.add(new JLabel(), c);
        c.weighty = 30;
        c.gridy = 5;
        mainPanel.add(new JLabel(), c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 3;
        c.gridheight = 1;
        mainPanel.add(imagePanel, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 2;
        c.gridwidth = 1;
        c.gridheight = 1;
        mainPanel.add(highscorePanel, c);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 1;
        c.gridy = 3;
        c.gridwidth = 1;
        c.gridheight = 1;
        mainPanel.add(left, c);
        left.setBackground(Color.RED);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 3;
        c.gridy = 3;
        c.gridwidth = 1;
        c.gridheight = 1;
        mainPanel.add(right, c);
        right.setBackground(Color.RED);

        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 2;
        c.gridy = 4;
        c.gridwidth = 1;
        c.gridheight = 1;
        mainPanel.add(back, c);

        add(mainPanel, BorderLayout.CENTER);
    }

    /**
     * The method getHighscorePanel returns the JPanel with highscores.
     * 
     * @param index
     *            specifies which board it will show highscores to
     * @return returns the JPanel with highscores
     */
    public JPanel getHighscorePanel(int index) {
        JPanel highscore = new JPanel();
        highscore.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        Connection con = null;
        try {
            con = getConnection();
            String query;
            PreparedStatement statement;
            ResultSet result;

            query = "SELECT username, time, score, bike, registered FROM highscore WHERE track = ? ORDER BY time LIMIT 10";
            statement = con.prepareStatement(query);
            statement.setString(1, "" + index);
            result = statement.executeQuery();

            c.gridx = 0;
            highscore.add(new JLabel("Skill"), c);
            c.gridx = 1;
            highscore.add(new JLabel("Username"), c);

            c.gridy = 0;

            while (result.next()) {

                String temp = result.getString(1);
                c.gridy++;
                c.gridx = 0;

                String query2 = "SELECT level FROM user WHERE username = ?";
                PreparedStatement statement2 = con.prepareStatement(query2);
                statement2.setString(1, temp);
                ResultSet result2 = statement2.executeQuery();
                while (result2.next()) {
                    JPanel panel = new JPanel();
                    panel.setBackground(Color.RED);
                    JLabel name = new JLabel("" + result2.getString(1));
                    panel.add(name);
                    highscore.add(panel, c);
                }

                c.gridx = 1;
                JPanel panel = new JPanel();
                panel.setBackground(Color.RED);
                JLabel name = new JLabel("" + temp);
                panel.add(name);
                highscore.add(panel, c);
            }
            c.gridx = 2;
            c.gridy = 0;

            highscore.add(new JLabel("Time"), c);

            result = statement.executeQuery();
            while (result.next()) {
                c.gridy++;
                JPanel panel = new JPanel();
                panel.setBackground(Color.RED);
                JLabel time = new JLabel("" + result.getString(2));
                panel.add(time);
                highscore.add(panel, c);
            }
            c.gridx = 3;
            c.gridy = 0;

            highscore.add(new JLabel("Score"), c);

            result = statement.executeQuery();
            while (result.next()) {
                c.gridy++;
                JPanel panel = new JPanel();
                panel.setBackground(Color.RED);
                JLabel score = new JLabel("" + result.getString(3));
                panel.add(score);
                highscore.add(panel, c);
            }

            c.gridx = 4;
            c.gridy = 0;

            highscore.add(new JLabel("Chopper"), c);

            result = statement.executeQuery();
            while (result.next()) {
                c.gridy++;
                JPanel panel = new JPanel();
                panel.setBackground(Color.RED);
                JLabel time = new JLabel("" + result.getString(4));
                panel.add(time);
                highscore.add(panel, c);
            }

            c.gridx = 5;
            c.gridy = 0;

            highscore.add(new JLabel("Registered"), c);

            result = statement.executeQuery();
            while (result.next()) {
                c.gridy++;
                JPanel panel = new JPanel();
                panel.setBackground(Color.RED);
                JLabel time = new JLabel("" + result.getString(5));
                panel.add(time);
                highscore.add(panel, c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null)
                    con.close();
            } catch (SQLException e) {
            }
        }
        highscore.setBackground(Color.RED);
        return highscore;
    }

    /**
     * The method getImagePanel creates a JPanel with an image based on @param
     * index
     * 
     * @param index
     *            specifies which image the panel should paint
     * @return returns a JPanel with the given image
     */
    public JPanel getImagePanel(int index) {
        JLabel image = new JLabel(new ImageIcon(boardPanel.getBoard(index)
                .getImage()));
        JPanel panel = new JPanel();
        panel.add(image);
        return panel;
    }

    public int getIndex() {
        return index;
    }

    /**
     * Overrides the superclass' paint method and paint the HighscorePanel
     */
    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, mainFrame.getMainMenu().getWidth(),
                mainFrame.getMainMenu().getHeight(), null);
        ((CardLayout) imagePanel.getLayout()).show(imagePanel, "" + index);
        ((CardLayout) highscorePanel.getLayout()).show(highscorePanel, ""
                + index);
        paintChildren(g);
    }

    /**
     * The method refreshHighscore refreshes the panel with the highscores.
     * 
     * @param index
     *            specifies which highscores that should be refreshed depending
     *            on the board.
     */
    public void refreshHighscore(int index) {
        JPanel panel = getHighscorePanel(index);
        highscorePanel.add(panel, "" + index);
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
