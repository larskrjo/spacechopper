package display;

import game.GamePanel;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import map.Board;
import menu.HighscorePanel;
import menu.MainMenu;
import sound.AudioPlayer;
import user.Chopper;

/**
 * The MainFrame is the extended JFrame the whole game is based on. Every
 * visible and invisible panel is attached to it, and the main method initiates
 * it.
 * 
 * MainFrame initiates a Menupanel and adds it to the MainFrames contentPane.
 * MainFrame also starts the music in the game when initialized.
 * 
 * @version 1.000, 01/05/09
 * @author Håvard Lødemel, Lars Kristian Johansen, Sigurd Wien, Håvard Malm
 *         Geithus
 * 
 */
public class MainFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    /**
     * This is the main method, and will run when you start the program. It
     * initiates the MainFrame's constructor.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame();
            }
        });
    }

    private AudioPlayer audioPlayer;
    private ArrayList<Board> boards = new ArrayList<Board>();
    private ArrayList<Chopper> choppers = new ArrayList<Chopper>();
    private GamePanel gamePanel;
    private int level = 1;
    private MainMenu menuPanel;

    private boolean onlineMode = true;

    private MainFrame() {
        if (HighscorePanel.getConnection() == null) {
            onlineMode = false;
        }

        setTitle("Space Chopper");

        UIManager.put("Button.select", new Color(0.4f, 0.8f, 0.2f));

        getContentPane().setLayout(new CardLayout());

        audioPlayer = new AudioPlayer();

        /** Creates the games 4 different choppers **/
        choppers.add(new Chopper(100, 0.95, 1, "Akselerator"));
        choppers.add(new Chopper(110, 0.94, 2, "Normal"));
        choppers.add(new Chopper(60, 0.93, 3, "Slowmo"));
        choppers.add(new Chopper(180, 0.92, 4, "Hyperman"));

        /** Creates 10 Boards **/
        for (int i = 1; i <= 10; i++) {
            boards.add(new Board(i, false));
        }
        getBoards().get(0).setAvailable(true);

        menuPanel = new MainMenu(this);
        menuPanel.setFocusable(true);

        getContentPane().add(menuPanel, "menu");

        /** Make the MainFrame go to fullscreen mode (and removes borders) */
        setUndecorated(true);
        GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getDefaultScreenDevice().setFullScreenWindow(this);
        audioPlayer.playMenuSound();
    }

    public AudioPlayer getAudioPlayer() {
        return audioPlayer;
    }

    public ArrayList<Board> getBoards() {
        return boards;
    }

    public ArrayList<Chopper> getChoppers() {
        return choppers;
    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

    public int getLevel() {
        return level;
    }

    public MainMenu getMainMenu() {
        return menuPanel;
    }

    public boolean isOnlineMode() {
        return onlineMode;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}