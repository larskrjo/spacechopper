package logic;

import map.Board;
import display.MainFrame;

/**
 * Game is initialized by the GamePanel and creates the current Board.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class Game {

    private Board board;
    private double gameStarted;
    private double gameEnded;
    private boolean gaming = true;

    public Game(MainFrame m) {
        board = m.getMainMenu().getBoardPanel().getCurrentBoard();
        gameStarted = System.currentTimeMillis();
    }

    public Board getBoard() {
        return board;
    }

    public double getGameEnded() {
        return gameEnded;
    }

    public double getGameStarted() {
        return gameStarted;
    }

    public boolean isGaming() {
        return gaming;
    }

    public void setGameEnded(double gameEnded) {
        this.gameEnded = gameEnded;
    }

    public void setGaming(boolean gaming) {
        this.gaming = gaming;
    }
}
