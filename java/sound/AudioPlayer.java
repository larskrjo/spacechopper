package sound;

import javazoom.jl.player.Player;

public class AudioPlayer {

    private Player gameSound;
    private Player menuSound;
    private Player crashSound;

    private Thread gameThread;
    private Thread menuThread;
    private Thread crashThread;

    private boolean muteGame;
    private boolean muteMenu;

    // constructor that takes the name of an MP3 file
    public AudioPlayer() {
        muteGame = false;
        muteMenu = false;
        createGameThread();
        createMenuThread();
        createJumpThread();
        createCrashThread();
    }

    public void playGameSound() {
        if (!gameThread.isAlive())
            createGameThread().start();
    }

    @SuppressWarnings("deprecation")
    public void stopGameSound() {
        if (gameThread.isAlive())
            gameThread.stop();
    }

    public void playMenuSound() {
        if (!menuThread.isAlive())
            createMenuThread().start();
    }

    @SuppressWarnings("deprecation")
    public void stopMenuSound() {
        if (menuThread.isAlive())
            menuThread.stop();
    }

    public void playJumpSound() {
        createJumpThread().start();
    }

    public void playCrashSound() {
        if (!crashThread.isAlive())
            createCrashThread().start();
    }

    private Thread createGameThread() {
        gameThread = new Thread() {
            public void run() {
                try {
                    while (!muteGame) {
                        gameSound = new Player(
                                AudioPlayer.class
                                        .getResourceAsStream("game.mp3"));
                        gameSound.play();
                        while (!gameSound.isComplete()) {
                            if (muteGame)
                                break;
                            Thread.sleep(1000);
                        }
                        gameSound.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        return gameThread;
    }

    private Thread createMenuThread() {
        menuThread = new Thread() {
            public void run() {
                try {
                    while (!muteMenu) {
                        menuSound = new Player(
                                AudioPlayer.class
                                        .getResourceAsStream("menu.mp3"));
                        menuSound.play();
                        while (!menuSound.isComplete()) {
                            if (muteMenu)
                                break;
                            Thread.sleep(1000);
                        }
                        menuSound.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        return menuThread;
    }

    private Thread createCrashThread() {
        crashThread = new Thread() {
            public void run() {
                try {
                    if (!muteGame) {
                        crashSound = new Player(
                                AudioPlayer.class
                                        .getResourceAsStream("crash.mp3"));
                        crashSound.play();
                        while (!crashSound.isComplete()) {
                            Thread.sleep(1000);
                        }
                        crashSound.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        return crashThread;
    }

    private Thread createJumpThread() {
        return new Thread() {
            public void run() {
                try {
                    if (!muteGame) {
                        Player jumpSound = new Player(
                                AudioPlayer.class
                                        .getResourceAsStream("jump.mp3"));
                        jumpSound.play();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public boolean isMuteGame() {
        return muteGame;
    }

    public boolean isMuteMenu() {
        return muteMenu;
    }

    public void setMuteGame(boolean muteGame) {
        this.muteGame = muteGame;
    }

    public void setMuteMenu(boolean muteMenu) {
        this.muteMenu = muteMenu;
    }
}
