package listener;

import game.GamePanel;

import java.awt.CardLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import display.MainFrame;

/**
 * MainMenuListener listens to MainMenu for keyboardinput
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class MainMenuListener implements KeyListener {
    MainFrame mainFrame;

    public MainMenuListener(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     * Defines hotkeys for exiting the game and for starting the currently
     * chosen board.
     */
    @Override
    public void keyPressed(KeyEvent arg0) {
        switch (arg0.getKeyCode()) {
        case KeyEvent.VK_F4:
            System.exit(0);
        case KeyEvent.VK_F1: {
            if (mainFrame.getGamePanel() != null) {
                mainFrame.getContentPane().remove(mainFrame.getGamePanel());
            }
            GamePanel gamePanel = new GamePanel(mainFrame);
            gamePanel.setFocusable(true);
            gamePanel.requestFocus();
            mainFrame.setGamePanel(gamePanel);
            mainFrame.getContentPane().add(gamePanel, "game");
            ((CardLayout) mainFrame.getContentPane().getLayout())
                    .next(mainFrame.getContentPane());
            mainFrame.getGamePanel().requestFocus();

            // mainFrame.getAudioPlayer().stop();
            // mainFrame.getAudioPlayer().setCurrentSong(1);
            // mainFrame.getAudioPlayer().getGameClip().setMicrosecondPosition(0);
            // mainFrame.getAudioPlayer().play();
            mainFrame.getAudioPlayer().stopMenuSound();
            mainFrame.getAudioPlayer().playGameSound();
        }
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
    }
}
