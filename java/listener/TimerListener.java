package listener;

import game.GamePanel;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.ImageIcon;

import menu.HighscorePanel;
import user.Chopper;

/**
 * TimerListener implements ActionListener, because the actionPerformed method
 * is invoked every 15ms by the Timer in GamePanel.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class TimerListener implements ActionListener {

    private GamePanel gamePanel;
    private double pos;
    private int timeLeft = 550;

    public TimerListener(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        pos = gamePanel.getPlayer().getPosition();
    }

    /**
     * Implements actionPerformed from the interface ActionListener, and this
     * method repaints the GamePanel.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (gamePanel.isIncreaseSpeed() ^ gamePanel.isDecreaseSpeed()) {
            if (gamePanel.isIncreaseSpeed()) {
                if (gamePanel.getSpeed() <= gamePanel.getPlayer().getChopper()
                        .getMaxSpeed()) {
                    if (gamePanel.getSpeed() < 1)
                        gamePanel.setSpeed(1);
                    else
                        gamePanel.setSpeed(gamePanel.getSpeed()
                                + (1 / gamePanel.getPlayer().getChopper()
                                        .getAcceleration()));
                }
            } else {
                if (gamePanel.getSpeed() >= 1)
                    gamePanel.setSpeed((int) (gamePanel.getPlayer()
                            .getChopper().getAcceleration() * gamePanel
                            .getSpeed()));
                else
                    gamePanel.setSpeed(0);
            }
        }
        if (gamePanel.isIncreaseAngel() ^ gamePanel.isDecreaseAngel()) {
            if (gamePanel.isIncreaseAngel()) {
                if (gamePanel.getDrawHeight() > 0.2) {
                    gamePanel.setDrawHeight(gamePanel.getDrawHeight() * 0.992);
                } else {
                    gamePanel.setIncreaseAngel(false);
                }
            } else {
                if (gamePanel.getDrawHeight() <= 0.8) {
                    gamePanel.setDrawHeight(gamePanel.getDrawHeight() * 1.008);
                } else {
                    gamePanel.setDecreaseAngel(false);
                }
            }
        }
        if (gamePanel.getMovingLeft() ^ gamePanel.getMovingRight()) {
            if (gamePanel.getMovingRight()) {
                if (gamePanel.getPlayer().getPositionX() < gamePanel
                        .getScreenWidth() - 130)
                    gamePanel.getPlayer().setPositionX(
                            gamePanel.getPlayer().getPositionX()
                                    + gamePanel.getMovement());
            } else {
                if (gamePanel.getPlayer().getPositionX() > 20)
                    gamePanel.getPlayer().setPositionX(
                            gamePanel.getPlayer().getPositionX()
                                    - gamePanel.getMovement());
            }
        }
        if (gamePanel.getPlayer().isJumping())
            gamePanel.getPlayer().calcPositionY();

        pos += gamePanel.getSpeed() / 10;
        gamePanel.getPlayer().setPosition((int) pos);
        if (!gamePanel.getGame().isGaming()) {
            if (gamePanel.isLost()) {
                if (!gamePanel
                        .getPlayer()
                        .getChopper()
                        .getBike()
                        .equals(new ImageIcon(Chopper.class
                                .getResource("explode.gif")).getImage())) {
                    gamePanel.getGame()
                            .setGameEnded(System.currentTimeMillis());
                    gamePanel
                            .getPlayer()
                            .getChopper()
                            .setBike(
                                    new ImageIcon(Chopper.class
                                            .getResource("explode.gif"))
                                            .getImage());
                    gamePanel.setHandling(false);
                    gamePanel.getMainFrame().getAudioPlayer().playCrashSound();
                    gamePanel.setPaintShadows(false);
                }
                if (timeLeft > 0) {
                    gamePanel.setDrawHeight(gamePanel.getDrawHeight() - 0.012);
                    timeLeft -= 10;
                } else {
                    timeLeft = 550;
                    gamePanel.setHandling(true);
                    gamePanel.setDrawHeight(0.6);
                    gamePanel
                            .getPlayer()
                            .getChopper()
                            .setBike(
                                    new ImageIcon(Chopper.class
                                            .getResource("bike.png"))
                                            .getImage());
                    gamePanel.getTimer().stop();
                    ((CardLayout) gamePanel.getMainFrame().getContentPane()
                            .getLayout()).next(gamePanel.getMainFrame()
                            .getContentPane());
                    ((CardLayout) gamePanel.getMainFrame().getMainMenu()
                            .getLayout()).show(gamePanel.getMainFrame()
                            .getMainMenu(), "menuPanel");
                    gamePanel.getMainFrame().getMainMenu().requestFocus();
                    // gamePanel.getMainFrame().getAudioPlayer().stop();
                    // gamePanel.getMainFrame().getAudioPlayer().getGameClip()
                    // .setMicrosecondPosition(0);
                    // gamePanel.getMainFrame().getAudioPlayer().setCurrentSong(0);
                    // gamePanel.getMainFrame().getAudioPlayer().run();
                    gamePanel.getMainFrame().getAudioPlayer().stopGameSound();
                    gamePanel.getMainFrame().getAudioPlayer().playMenuSound();
                    gamePanel.setPaintShadows(true);
                }
            } else if (gamePanel.isWon()) {
                if (gamePanel.isHandling() != false) {
                    gamePanel.setHandling(false);
                    gamePanel.setPaintShadows(false);
                    gamePanel.getGame()
                            .setGameEnded(System.currentTimeMillis());
                    try {
                        Connection con;
                        String query;
                        PreparedStatement statement;
                        if (gamePanel.getMainFrame().isOnlineMode()) {
                            // Insert the record
                            con = HighscorePanel.getConnection();
                            query = "INSERT INTO highscore (username, time, score, track, registered, bike) VALUES(?, ?, ?, ?, now(), ?)";
                            statement = con.prepareStatement(query);
                            statement.setString(1, gamePanel.getMainFrame()
                                    .getMainMenu().getMenuPanel()
                                    .getPlayerName());
                            statement
                                    .setString(
                                            2,
                                            ""
                                                    + ((gamePanel.getGame()
                                                            .getGameEnded() - gamePanel
                                                            .getGame()
                                                            .getGameStarted()) / 100)
                                                    / 10);
                            statement
                                    .setString(
                                            3,
                                            ""
                                                    + (1000000.0 / (System
                                                            .currentTimeMillis() - gamePanel
                                                            .getGame()
                                                            .getGameStarted())));
                            statement.setString(4, ""
                                    + gamePanel.getMainFrame().getMainMenu()
                                            .getBoardPanel().getBoardIndex());
                            statement.setString(
                                    5,
                                    gamePanel
                                            .getMainFrame()
                                            .getChoppers()
                                            .get(gamePanel.getMainFrame()
                                                    .getMainMenu()
                                                    .getChopperPanel()
                                                    .getCurrentChopperIndex())
                                            .getName());
                            statement.executeUpdate();
                        }
                        boolean upgradeLevel = false;
                        // Checks if this map is the hardest map, and increase
                        // the level
                        if (gamePanel.getMainFrame().isOnlineMode()) {
                            gamePanel
                                    .getMainFrame()
                                    .getMainMenu()
                                    .getHighscorePanel()
                                    .refreshHighscore(
                                            gamePanel.getMainFrame()
                                                    .getMainMenu()
                                                    .getHighscorePanel()
                                                    .getIndex());
                            gamePanel
                                    .getMainFrame()
                                    .getMainMenu()
                                    .getHighscorePanel()
                                    .setIndex(
                                            gamePanel.getMainFrame()
                                                    .getMainMenu()
                                                    .getBoardPanel()
                                                    .getBoardIndex());
                            query = "SELECT level FROM user WHERE username = ?";
                            con = HighscorePanel.getConnection();
                            statement = con.prepareStatement(query);
                            statement.setString(1, gamePanel.getMainFrame()
                                    .getMainMenu().getMenuPanel()
                                    .getPlayerName());
                            ResultSet rs = statement.executeQuery();
                            while (rs.next()) {
                                if (rs.getInt(1) - 1 == gamePanel
                                        .getMainFrame().getMainMenu()
                                        .getBoardPanel().getBoardIndex()
                                        && gamePanel.getMainFrame()
                                                .getMainMenu().getBoardPanel()
                                                .getBoardIndex() != 9) {
                                    upgradeLevel = true;
                                }
                            }
                        } else if (gamePanel.getMainFrame().getLevel() - 1 == gamePanel
                                .getMainFrame().getMainMenu().getBoardPanel()
                                .getBoardIndex()
                                && gamePanel.getMainFrame().getMainMenu()
                                        .getBoardPanel().getBoardIndex() != 9) {
                            upgradeLevel = true;
                        }
                        if (upgradeLevel) {
                            if (gamePanel.getMainFrame().isOnlineMode()) {
                                query = "UPDATE `prosjekt`.`user` SET level = ? WHERE username = ?";
                                con = HighscorePanel.getConnection();
                                statement = con.prepareStatement(query);
                                statement.setString(1, ""
                                        + (gamePanel.getMainFrame()
                                                .getMainMenu().getBoardPanel()
                                                .getBoardIndex() + 2));
                                statement.setString(2, gamePanel.getMainFrame()
                                        .getMainMenu().getMenuPanel()
                                        .getPlayerName());
                                statement.executeUpdate();
                                gamePanel
                                        .getMainFrame()
                                        .getBoards()
                                        .get(gamePanel.getMainFrame()
                                                .getMainMenu().getBoardPanel()
                                                .getBoardIndex() + 1)
                                        .setAvailable(true);
                                gamePanel.getMainFrame().getMainMenu()
                                        .getBoardPanel().refreshBoards();
                            } else {
                                gamePanel.getMainFrame()
                                        .setLevel(
                                                gamePanel.getMainFrame()
                                                        .getLevel() + 1);
                                gamePanel
                                        .getMainFrame()
                                        .getBoards()
                                        .get(gamePanel.getMainFrame()
                                                .getMainMenu().getBoardPanel()
                                                .getBoardIndex() + 1)
                                        .setAvailable(true);
                                gamePanel.getMainFrame().getMainMenu()
                                        .getBoardPanel().refreshBoards();
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                if (timeLeft > 0) {
                    gamePanel.setDrawHeight(gamePanel.getDrawHeight() - 0.012);
                    timeLeft -= 10;
                } else {
                    timeLeft = 550;
                    gamePanel.setHandling(true);
                    gamePanel.setDrawHeight(0.6);
                    gamePanel
                            .getPlayer()
                            .getChopper()
                            .setBike(
                                    new ImageIcon(Chopper.class
                                            .getResource("bike.png"))
                                            .getImage());
                    gamePanel.getTimer().stop();
                    gamePanel.getMainFrame().getMainMenu().requestFocus();
                    gamePanel.getMainFrame().getAudioPlayer().stopGameSound();
                    gamePanel.getMainFrame().getAudioPlayer().playMenuSound();
                    gamePanel.setPaintShadows(true);

                    ((CardLayout) gamePanel.getMainFrame().getContentPane()
                            .getLayout()).next(gamePanel.getMainFrame()
                            .getContentPane());
                    if (gamePanel.getMainFrame().isOnlineMode()) {
                        ((CardLayout) gamePanel.getMainFrame().getMainMenu()
                                .getLayout()).show(gamePanel.getMainFrame()
                                .getMainMenu(), "highscorePanel");
                    } else {
                        ((CardLayout) gamePanel.getMainFrame().getMainMenu()
                                .getLayout()).show(gamePanel.getMainFrame()
                                .getMainMenu(), "menuPanel");
                    }
                }
            }
            if (!gamePanel.getTimer().isRunning()) {
                gamePanel.setLost(false);
                gamePanel.setWon(false);
            }
        }
        gamePanel.repaint();
    }
}