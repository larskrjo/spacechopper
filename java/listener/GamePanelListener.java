package listener;

import game.GamePanel;

import java.awt.CardLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;

import user.Chopper;
import display.MainFrame;

/**
 * GamePanelListener listens to GamePanel for keyboardinput
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class GamePanelListener implements KeyListener {
    private GamePanel gamePanel;
    private boolean hasJumped = false;
    private MainFrame mainFrame;

    public GamePanelListener(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        mainFrame = gamePanel.getMainFrame();
    }

    /**
     * Sets booleans values to true, and depending on their value, different
     * actions will happen. The difference between performing these events
     * directly, and via variables like this, is that this way does not depend
     * as much on the OS' handling of keyinput.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if (gamePanel.isHandling()) {
            switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                gamePanel.setDecreaseSpeed(true);
                break;
            case KeyEvent.VK_UP:
                gamePanel.setIncreaseSpeed(true);
                break;
            case KeyEvent.VK_Z:
                gamePanel.setIncreaseAngel(true);
                break;
            case KeyEvent.VK_X:
                gamePanel.setDecreaseAngel(true);
                break;
            case KeyEvent.VK_LEFT:
                if (gamePanel.getPlayer().getPositionX() > -25) {
                    gamePanel.setMovingLeft(true);
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (gamePanel.getPlayer().getPositionX() < gamePanel.getWidth() - 130) {
                    gamePanel.setMovingRight(true);
                }
                break;
            case KeyEvent.VK_SPACE:
                if (!hasJumped) {
                    gamePanel.getPlayer().jump();
                    hasJumped = true;
                }
                break;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            gamePanel.getTimer().stop();
            gamePanel
                    .getPlayer()
                    .getChopper()
                    .setBike(
                            new ImageIcon(Chopper.class.getResource("bike.png"))
                                    .getImage());
            ((CardLayout) mainFrame.getContentPane().getLayout())
                    .next(mainFrame.getContentPane());
            ((CardLayout) mainFrame.getMainMenu().getLayout()).show(
                    mainFrame.getMainMenu(), "menuPanel");
            mainFrame.getMainMenu().requestFocus();
            mainFrame.getAudioPlayer().stopGameSound();
            mainFrame.getAudioPlayer().playMenuSound();
        }
    }

    /**
     * Sets the boolean variables corresponding to the keyinput to false.
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (gamePanel.isHandling()) {
            switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                gamePanel.setMovingLeft(false);
                break;
            case KeyEvent.VK_RIGHT:
                gamePanel.setMovingRight(false);
                break;
            case KeyEvent.VK_UP:
                gamePanel.setIncreaseSpeed(false);
                break;
            case KeyEvent.VK_DOWN:
                gamePanel.setDecreaseSpeed(false);
                break;
            case KeyEvent.VK_X:
                gamePanel.setDecreaseAngel(false);
                break;
            case KeyEvent.VK_Z:
                gamePanel.setIncreaseAngel(false);
                break;
            case KeyEvent.VK_SPACE:
                hasJumped = false;
                break;
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
}