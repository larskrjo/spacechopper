package map;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * Pillar is initialized by the Board object and it creates a list of intervals.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class Pillar {

    private List<Interval> intervals = new ArrayList<Interval>();
    private List<String> name = new ArrayList<String>();

    public Pillar(int number, int board) {
        if (name.size() == 0) {
            name.add("Board 1");
            name.add("Board 2");
            name.add("Board 3");
            name.add("Board 4");
            name.add("Board 5");
            name.add("Board 6");
            name.add("Board 7");
            name.add("Board 8");
            name.add("Board 9");
            name.add("Board 10");
        }
        if (board == 1) {
            switch (number) {
            // Brett 1
            case 0:
                intervals.add(new Interval(2700, 3300));

                intervals.add(new Interval(5550, 5700, Color.CYAN));
                break;
            case 1:
                intervals.add(new Interval(2700, 3300));

                intervals.add(new Interval(5550, 5700, Color.CYAN));
                break;
            case 2:
                intervals.add(new Interval(2400, 3000));
                break;
            case 3:
                intervals.add(new Interval(2100, 2700));

                intervals.add(new Interval(3400, 3800));

                intervals.add(new Interval(5100, 5400));
                break;
            case 4:
                intervals.add(new Interval(1800, 2400));

                intervals.add(new Interval(3400, 3800));

                intervals.add(new Interval(5100, 5400));
                break;
            case 5:
                intervals.add(new Interval(0, 400));
                intervals.add(new Interval(1500, 2100));
                break;
            case 6:
                intervals.add(new Interval(0, 400));
                intervals.add(new Interval(1200, 1800));

                intervals.add(new Interval(3900, 4300));

                intervals.add(new Interval(4700, 5000));
                break;
            case 7:
                intervals.add(new Interval(400, 700));
                intervals.add(new Interval(900, 1500));

                intervals.add(new Interval(3900, 4300));

                intervals.add(new Interval(4700, 5000));
                break;
            case 8:
                intervals.add(new Interval(400, 900));
                intervals.add(new Interval(900, 1200));
                break;
            case 9:
                intervals.add(new Interval(700, 900));

                intervals.add(new Interval(4400, 4600));
                break;

            }
        } else if (board == 2) {
            switch (number) {
            // Brett 2
            case 0:
                intervals.add(new Interval(6600, 7000, Color.cyan));
                break;
            case 1:
                intervals.add(new Interval(2150, 2500));
                intervals.add(new Interval(2650, 3000));
                intervals.add(new Interval(5200, 6000, Color.red));
                intervals.add(new Interval(6600, 7000, Color.cyan));
                break;
            case 2:
                intervals.add(new Interval(600, 800));
                intervals.add(new Interval(950, 1200));
                intervals.add(new Interval(2150, 2500));
                intervals.add(new Interval(2650, 3000));
                intervals.add(new Interval(4700, 5000));
                intervals.add(new Interval(5200, 6000, Color.red));
                intervals.add(new Interval(6400, 6550));
                break;
            case 3:
                intervals.add(new Interval(500, 700));
                intervals.add(new Interval(1300, 1600));
                intervals.add(new Interval(2750, 3200));
                intervals.add(new Interval(4700, 5000));
                intervals.add(new Interval(5200, 6000, Color.red));
                intervals.add(new Interval(6200, 6350));
                intervals.add(new Interval(6400, 6550));
                break;
            case 4:
                intervals.add(new Interval(0, 700));
                intervals.add(new Interval(1000, 1200, Color.red));
                intervals.add(new Interval(1500, 2000, Color.blue));
                intervals.add(new Interval(2750, 3200));
                intervals.add(new Interval(3350, 3550));
                intervals.add(new Interval(3750, 4200));
                intervals.add(new Interval(4450, 4600));
                intervals.add(new Interval(5200, 6000, Color.blue));
                intervals.add(new Interval(6200, 6350));
                break;
            case 5:
                intervals.add(new Interval(0, 700));
                intervals.add(new Interval(1000, 1200, Color.red));
                intervals.add(new Interval(1500, 2000, Color.blue));
                intervals.add(new Interval(2750, 3200));
                intervals.add(new Interval(3350, 3550));
                intervals.add(new Interval(3750, 4200));
                intervals.add(new Interval(4450, 4600));
                intervals.add(new Interval(5200, 6000, Color.blue));
                intervals.add(new Interval(6100, 7000));
                intervals.add(new Interval(7000, 7500, Color.cyan));
                break;
            case 6:
                intervals.add(new Interval(500, 700));
                intervals.add(new Interval(1300, 1600));
                intervals.add(new Interval(2750, 3200));
                intervals.add(new Interval(4700, 5000));
                intervals.add(new Interval(5200, 6000, Color.red));
                intervals.add(new Interval(6100, 7000));
                intervals.add(new Interval(7000, 7500, Color.cyan));
                break;
            case 7:
                intervals.add(new Interval(600, 800));
                intervals.add(new Interval(950, 1200));
                intervals.add(new Interval(2150, 2500));
                intervals.add(new Interval(2650, 3000));
                intervals.add(new Interval(4700, 5000));
                intervals.add(new Interval(5200, 6000, Color.red));
                break;
            case 8:
                intervals.add(new Interval(2150, 2500));
                intervals.add(new Interval(2650, 3000));
                intervals.add(new Interval(5200, 6000, Color.red));
                break;
            case 9:
                break;
            }
        } else if (board == 3) {
            switch (number) {
            // Brett 3
            case 0:
                intervals.add(new Interval(3600, 4200, Color.red));
                intervals.add(new Interval(4300, 4900, Color.red));
                break;
            case 1:
                intervals.add(new Interval(3600, 4200, Color.red));
                intervals.add(new Interval(4300, 4900, Color.red));
                break;
            case 2:
                intervals.add(new Interval(150, 250));
                intervals.add(new Interval(400, 600));
                intervals.add(new Interval(300, 350));
                intervals.add(new Interval(1850, 2050));
                intervals.add(new Interval(2170, 2400));
                intervals.add(new Interval(3000, 3200));
                intervals.add(new Interval(3875, 3925, Color.red));
                intervals.add(new Interval(4300, 4900, Color.red));
                break;
            case 3:
                intervals.add(new Interval(100, 200));
                intervals.add(new Interval(550, 650));
                intervals.add(new Interval(1850, 2050));
                intervals.add(new Interval(2170, 2400));
                intervals.add(new Interval(3000, 3200));
                intervals.add(new Interval(3400, 3875));
                intervals.add(new Interval(3875, 3925, Color.red));
                intervals.add(new Interval(3925, 4300));
                intervals.add(new Interval(4300, 4400, Color.red));
                intervals.add(new Interval(4400, 5000));
                break;
            case 4:
                intervals.add(new Interval(50, 150));
                intervals.add(new Interval(600, 750));
                intervals.add(new Interval(870, 1300, Color.blue));
                intervals.add(new Interval(1300, 1500, Color.red));
                intervals.add(new Interval(1500, 1700, Color.blue));
                intervals.add(new Interval(2600, 2800));
                intervals.add(new Interval(3400, 3875));
                intervals.add(new Interval(3875, 3925, Color.red));
                intervals.add(new Interval(3925, 4300));
                intervals.add(new Interval(4300, 4400, Color.red));
                intervals.add(new Interval(4400, 5000));
                intervals.add(new Interval(5000, 5300, Color.blue));
                intervals.add(new Interval(5520, 5800, Color.black));
                intervals.add(new Interval(5800, 6000, Color.cyan));
                break;
            case 5:
                intervals.add(new Interval(0, 100));
                intervals.add(new Interval(870, 1300, Color.blue));
                intervals.add(new Interval(1300, 1500, Color.red));
                intervals.add(new Interval(1500, 1700, Color.blue));
                intervals.add(new Interval(2600, 2800));
                intervals.add(new Interval(3400, 3875));
                intervals.add(new Interval(3875, 3925, Color.red));
                intervals.add(new Interval(3925, 4300));
                intervals.add(new Interval(4300, 4400, Color.red));
                intervals.add(new Interval(4400, 5000));
                intervals.add(new Interval(5000, 5300, Color.blue));
                intervals.add(new Interval(5520, 5800, Color.black));
                intervals.add(new Interval(5800, 6000, Color.cyan));
                break;
            case 6:
                intervals.add(new Interval(50, 150));
                intervals.add(new Interval(600, 750));
                intervals.add(new Interval(870, 1300, Color.blue));
                intervals.add(new Interval(1300, 1500, Color.red));
                intervals.add(new Interval(1500, 1700, Color.blue));
                intervals.add(new Interval(2600, 2800));
                intervals.add(new Interval(3875, 3925, Color.red));
                intervals.add(new Interval(4300, 4400, Color.red));
                break;
            case 7:
                intervals.add(new Interval(100, 200));
                intervals.add(new Interval(550, 650));
                intervals.add(new Interval(1850, 2050));
                intervals.add(new Interval(2170, 2400));
                intervals.add(new Interval(3000, 3200));
                intervals.add(new Interval(3875, 3925, Color.red));
                intervals.add(new Interval(4300, 4400, Color.red));
                break;
            case 8:
                intervals.add(new Interval(150, 250));
                intervals.add(new Interval(400, 600));
                intervals.add(new Interval(300, 350));
                intervals.add(new Interval(1850, 2050));
                intervals.add(new Interval(2170, 2400));
                intervals.add(new Interval(3000, 3200));
                intervals.add(new Interval(3600, 4200, Color.red));
                intervals.add(new Interval(4300, 4400, Color.red));
                break;
            case 9:
                intervals.add(new Interval(3600, 4200, Color.red));
                intervals.add(new Interval(4300, 4400, Color.red));
                break;
            }
        } else if (board == 4) {
            switch (number) {
            // Brett 4

            case 0:
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(2400, 2550));
                intervals.add(new Interval(3000, 3150));
                intervals.add(new Interval(3600, 3750));
                intervals.add(new Interval(3900, 4000, Color.BLUE));
                break;
            case 1:
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(2400, 2550));
                intervals.add(new Interval(3000, 3150));
                intervals.add(new Interval(3600, 3750));
                intervals.add(new Interval(3900, 4000, Color.BLUE));
                intervals.add(new Interval(4200, 4400, Color.BLUE));
                break;
            case 2:
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(2400, 2550));
                intervals.add(new Interval(3000, 3150));
                intervals.add(new Interval(3600, 3750));
                intervals.add(new Interval(3900, 4000, Color.BLUE));
                intervals.add(new Interval(4200, 4400, Color.BLUE));
                intervals.add(new Interval(4600, 4800, Color.BLUE));
                break;
            case 3:
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(2100, 2250));
                intervals.add(new Interval(2700, 2850));
                intervals.add(new Interval(3300, 3450));
                intervals.add(new Interval(3900, 4000, Color.BLUE));
                intervals.add(new Interval(4200, 4400, Color.BLUE));
                intervals.add(new Interval(4600, 4800, Color.BLUE));
                intervals.add(new Interval(5000, 5200, Color.BLUE));
                break;
            case 4:
                intervals.add(new Interval(0, 200));
                intervals.add(new Interval(600, 750));
                intervals.add(new Interval(1200, 1350));
                intervals.add(new Interval(1650, 1800, Color.RED));
                intervals.add(new Interval(2100, 2250));
                intervals.add(new Interval(2700, 2850));
                intervals.add(new Interval(3300, 3450));
                intervals.add(new Interval(3900, 4000, Color.BLUE));
                intervals.add(new Interval(4600, 4800, Color.BLUE));
                intervals.add(new Interval(5000, 5200, Color.BLUE));
                intervals.add(new Interval(5400, 5550, Color.BLUE));
                intervals.add(new Interval(5550, 5600, Color.RED));
                break;
            case 5:
                intervals.add(new Interval(0, 200));
                intervals.add(new Interval(600, 750));
                intervals.add(new Interval(1200, 1350));
                intervals.add(new Interval(1650, 1800, Color.RED));
                intervals.add(new Interval(2100, 2250));
                intervals.add(new Interval(2700, 2850));
                intervals.add(new Interval(3300, 3450));
                intervals.add(new Interval(3900, 4000, Color.BLUE));
                intervals.add(new Interval(5000, 5200, Color.BLUE));
                intervals.add(new Interval(5400, 5550, Color.BLUE));
                intervals.add(new Interval(5550, 5600, Color.RED));
                intervals.add(new Interval(5800, 6000, Color.BLUE));
                break;
            case 6:
                intervals.add(new Interval(300, 450));
                intervals.add(new Interval(900, 1050));
                intervals.add(new Interval(1500, 1650));
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(5400, 5550, Color.BLUE));
                intervals.add(new Interval(5550, 5600, Color.RED));
                intervals.add(new Interval(5800, 6000, Color.BLUE));
                intervals.add(new Interval(6200, 6400, Color.BLUE));
                break;
            case 7:
                intervals.add(new Interval(300, 450));
                intervals.add(new Interval(900, 1050));
                intervals.add(new Interval(1500, 1650));
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(5800, 6000, Color.BLUE));
                intervals.add(new Interval(6200, 6400, Color.BLUE));
                intervals.add(new Interval(6600, 6800, Color.BLUE));
                intervals.add(new Interval(6800, 7000, Color.RED));
                intervals.add(new Interval(7000, 7800, Color.CYAN));
                break;
            case 8:
                intervals.add(new Interval(300, 450));
                intervals.add(new Interval(900, 1050));
                intervals.add(new Interval(1500, 1650));
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(6200, 6400, Color.BLUE));
                intervals.add(new Interval(6600, 6800, Color.BLUE));
                intervals.add(new Interval(6800, 7000, Color.RED));
                intervals.add(new Interval(7000, 7800, Color.CYAN));
                break;
            case 9:
                intervals.add(new Interval(300, 450));
                intervals.add(new Interval(900, 1050));
                intervals.add(new Interval(1500, 1650));
                intervals.add(new Interval(1800, 1950));
                intervals.add(new Interval(6600, 6800, Color.BLUE));
                intervals.add(new Interval(6800, 7000, Color.RED));
                intervals.add(new Interval(7000, 7800, Color.CYAN));
                break;
            }
        } else if (board == 5) {
            switch (number) {
            // Brett 5

            case 0:
                break;
            case 1:
                break;
            case 2:
                intervals.add(new Interval(1950, 2100, Color.RED));
                intervals.add(new Interval(2100, 2250));
                break;
            case 3:
                intervals.add(new Interval(1950, 2100, Color.RED));
                intervals.add(new Interval(2100, 2250));

                intervals.add(new Interval(2800, 3000));
                intervals.add(new Interval(3000, 3200, Color.RED));
                intervals.add(new Interval(3200, 3400, Color.BLUE));
                intervals.add(new Interval(3400, 3600, Color.RED));

                intervals.add(new Interval(3700, 3900));
                intervals.add(new Interval(3900, 4100, Color.RED));
                intervals.add(new Interval(4100, 4300));
                intervals.add(new Interval(4300, 4500, Color.RED));

                intervals.add(new Interval(4550, 5000, Color.CYAN));
                break;
            case 4:
                intervals.add(new Interval(0, 400, Color.RED));

                intervals.add(new Interval(2800, 3000));
                intervals.add(new Interval(3000, 3200, Color.RED));
                intervals.add(new Interval(3200, 3400, Color.BLUE));
                intervals.add(new Interval(3400, 3600, Color.RED));

                intervals.add(new Interval(3700, 3900));
                intervals.add(new Interval(3900, 4100, Color.RED));
                intervals.add(new Interval(4100, 4300));
                intervals.add(new Interval(4300, 4500, Color.RED));

                intervals.add(new Interval(4550, 5000, Color.CYAN));
                break;
            case 5:
                intervals.add(new Interval(0, 200, Color.BLUE));
                intervals.add(new Interval(300, 450));
                intervals.add(new Interval(600, 750));
                intervals.add(new Interval(900, 1050));
                intervals.add(new Interval(1200, 1350));
                intervals.add(new Interval(1500, 1650));
                intervals.add(new Interval(1650, 1800, Color.RED));
                intervals.add(new Interval(1800, 1950));
                break;
            case 6:
                intervals.add(new Interval(0, 400, Color.RED));

                intervals.add(new Interval(1200, 1350));
                intervals.add(new Interval(1500, 1650));
                intervals.add(new Interval(1650, 1800, Color.RED));
                intervals.add(new Interval(1800, 1950));

                intervals.add(new Interval(2400, 2450, Color.RED));
                intervals.add(new Interval(2450, 2700));
                break;
            case 7:
                intervals.add(new Interval(2400, 2450, Color.RED));
                intervals.add(new Interval(2450, 2700));
                break;
            case 8:
                break;
            case 9:
                break;
            }
        } else if (board == 6) {
            // Brett 6
            switch (number) {

            case 0:
                intervals.add(new Interval(0, 200));
                intervals.add(new Interval(4500, 4900));
                intervals.add(new Interval(5130, 5300));
                intervals.add(new Interval(5500, 5700));
                intervals.add(new Interval(5800, 6200));
                intervals.add(new Interval(6400, 6600));
                intervals.add(new Interval(8800, 9900));
                intervals.add(new Interval(9900, 10000, Color.CYAN));
                break;
            case 1:
                intervals.add(new Interval(0, 400));
                intervals.add(new Interval(4500, 4900));
                intervals.add(new Interval(5130, 5300));
                intervals.add(new Interval(5500, 5700));
                intervals.add(new Interval(5800, 5900));
                intervals.add(new Interval(6400, 6600));
                intervals.add(new Interval(8800, 9900));
                intervals.add(new Interval(9900, 10000, Color.CYAN));
                break;
            case 2:
                intervals.add(new Interval(0, 600));
                intervals.add(new Interval(1200, 1500, Color.BLUE));
                intervals.add(new Interval(1700, 2000));
                intervals.add(new Interval(2800, 3200, Color.RED));
                intervals.add(new Interval(4000, 4020, Color.RED));
                intervals.add(new Interval(4020, 4300));
                intervals.add(new Interval(6800, 7000));
                intervals.add(new Interval(8400, 8430, Color.RED));
                intervals.add(new Interval(8430, 8600));
                break;
            case 3:
                intervals.add(new Interval(0, 800));
                intervals.add(new Interval(1200, 1500, Color.BLUE));
                intervals.add(new Interval(1700, 2000));
                intervals.add(new Interval(2800, 3200, Color.RED));
                intervals.add(new Interval(4000, 4020, Color.RED));
                intervals.add(new Interval(4020, 4300));
                intervals.add(new Interval(5800, 6200));
                intervals.add(new Interval(6800, 7000));
                intervals.add(new Interval(8400, 8430, Color.RED));
                intervals.add(new Interval(8430, 8600));
                break;
            case 4: // MIDDLE
                intervals.add(new Interval(0, 1000));
                intervals.add(new Interval(1000, 1050, Color.RED));
                intervals.add(new Interval(1100, 1150, Color.RED));
                intervals.add(new Interval(1200, 1250, Color.RED));
                intervals.add(new Interval(2200, 2600));
                intervals.add(new Interval(3400, 3800));
                intervals.add(new Interval(5800, 5900));
                intervals.add(new Interval(6100, 6200));
                intervals.add(new Interval(7200, 7400));
                intervals.add(new Interval(8000, 8200));
                break;
            case 5: // MIDDLE
                intervals.add(new Interval(0, 1000));
                intervals.add(new Interval(1000, 1050, Color.RED));
                intervals.add(new Interval(1100, 1150, Color.RED));
                intervals.add(new Interval(1200, 1250, Color.RED));
                intervals.add(new Interval(2200, 2600));
                intervals.add(new Interval(3400, 3800));
                intervals.add(new Interval(5800, 6200));
                intervals.add(new Interval(7200, 7400));
                intervals.add(new Interval(8000, 8200));
                break;
            case 6:
                intervals.add(new Interval(0, 800));
                intervals.add(new Interval(1200, 1500, Color.BLUE));

                intervals.add(new Interval(1700, 2000));
                intervals.add(new Interval(2800, 3200, Color.BLUE));
                intervals.add(new Interval(4000, 4400, Color.BLUE));
                intervals.add(new Interval(7600, 7800));
                intervals.add(new Interval(8400, 8600, Color.BLUE));
                break;
            case 7:
                intervals.add(new Interval(0, 600));
                intervals.add(new Interval(1200, 1500, Color.BLUE));
                intervals.add(new Interval(1700, 2000));
                intervals.add(new Interval(2800, 3200, Color.BLUE));
                intervals.add(new Interval(4000, 4800, Color.BLUE));
                intervals.add(new Interval(5800, 6200, Color.RED));
                intervals.add(new Interval(7600, 7800));
                intervals.add(new Interval(8400, 8600, Color.BLUE));
                break;
            case 8:
                intervals.add(new Interval(0, 400));
                intervals.add(new Interval(4000, 5600, Color.BLUE));
                intervals.add(new Interval(5800, 5900, Color.RED));
                intervals.add(new Interval(8800, 8970));
                intervals.add(new Interval(8970, 9000, Color.RED));
                break;
            case 9:
                intervals.add(new Interval(0, 200));
                intervals.add(new Interval(4000, 5600, Color.BLUE));
                intervals.add(new Interval(8800, 8970));
                intervals.add(new Interval(8970, 9000, Color.RED));
                break;

            }
        } else if (board == 7) {
            switch (number) {
            // Brett 7

            case 0:
                intervals.add(new Interval(2100, 2450));
                intervals.add(new Interval(2600, 2850, Color.RED));
                break;
            case 1:
                intervals.add(new Interval(1100, 1300));
                intervals.add(new Interval(1350, 1400, Color.RED));
                intervals.add(new Interval(1450, 1700, Color.BLUE));
                intervals.add(new Interval(1800, 2000));
                intervals.add(new Interval(3000, 3200, Color.BLUE));
                intervals.add(new Interval(5300, 5800));
                intervals.add(new Interval(5900, 6100, Color.RED));
                intervals.add(new Interval(6850, 7200));
                intervals.add(new Interval(7300, 7600));
                break;
            case 2:
                intervals.add(new Interval(3300, 3500));
                intervals.add(new Interval(3500, 3550, Color.RED));
                break;
            case 3:
                intervals.add(new Interval(0, 500));
                intervals.add(new Interval(650, 1000));
                intervals.add(new Interval(4470, 4800, Color.BLUE));
                intervals.add(new Interval(4950, 5200));
                intervals.add(new Interval(6250, 6500));
                intervals.add(new Interval(6600, 6700));
                intervals.add(new Interval(7750, 8000));
                break;
            case 4:
                intervals.add(new Interval(0, 500));
                intervals.add(new Interval(650, 1000));
                intervals.add(new Interval(3600, 3900));
                intervals.add(new Interval(3900, 3950, Color.RED));
                intervals.add(new Interval(4050, 4300, Color.BLUE));
                intervals.add(new Interval(8150, 8500));
                intervals.add(new Interval(8600, 8800));
                intervals.add(new Interval(8950, 9200));
                intervals.add(new Interval(9370, 9500, Color.CYAN));
                break;
            case 5:
                intervals.add(new Interval(0, 500));
                intervals.add(new Interval(650, 1000));
                intervals.add(new Interval(3600, 3900));
                intervals.add(new Interval(3900, 3950, Color.RED));
                intervals.add(new Interval(4050, 4300, Color.BLUE));
                intervals.add(new Interval(8150, 8500));
                intervals.add(new Interval(8600, 8800));
                intervals.add(new Interval(8950, 9200));
                intervals.add(new Interval(9370, 9500, Color.CYAN));
                break;
            case 6:
                intervals.add(new Interval(0, 500));
                intervals.add(new Interval(650, 1000));
                intervals.add(new Interval(4470, 4800, Color.BLUE));
                intervals.add(new Interval(4950, 5200));
                intervals.add(new Interval(6250, 6500));
                intervals.add(new Interval(6600, 6700));
                intervals.add(new Interval(7750, 8000));
                break;
            case 7:
                intervals.add(new Interval(3300, 3500));
                intervals.add(new Interval(3500, 3550, Color.RED));
                break;
            case 8:
                intervals.add(new Interval(1100, 1300));
                intervals.add(new Interval(1350, 1400, Color.RED));
                intervals.add(new Interval(1450, 1700, Color.BLUE));
                intervals.add(new Interval(1800, 2000));
                intervals.add(new Interval(3000, 3200, Color.BLUE));
                intervals.add(new Interval(5300, 5800));
                intervals.add(new Interval(5900, 6100, Color.BLUE));
                intervals.add(new Interval(6850, 7200));
                break;
            case 9:
                intervals.add(new Interval(2100, 2450));
                intervals.add(new Interval(2600, 2850));
                break;
            }
        } else if (board == 8) {
            switch (number) {
            // Brett 8

            case 0:
                intervals.add(new Interval(5650, 6000, Color.BLUE));
                intervals.add(new Interval(6150, 6500));
                break;
            case 1:
                intervals.add(new Interval(5100, 5200));
                intervals.add(new Interval(5200, 5300, Color.RED));
                intervals.add(new Interval(5350, 5500));
                intervals.add(new Interval(5500, 5600, Color.RED));
                break;
            case 2:
                intervals.add(new Interval(4750, 5000));
                intervals.add(new Interval(5100, 5500, Color.RED));
                intervals.add(new Interval(6650, 6800));
                break;
            case 3:
                intervals.add(new Interval(400, 700, Color.RED));
                intervals.add(new Interval(3650, 4000, Color.BLUE));
                intervals.add(new Interval(4100, 4200));
                intervals.add(new Interval(4300, 4400));
                intervals.add(new Interval(4500, 4600));
                intervals.add(new Interval(6950, 7100));
                break;
            case 4:
                intervals.add(new Interval(1200, 1500));
                intervals.add(new Interval(3000, 3500));
                intervals.add(new Interval(3500, 3700, Color.RED));
                break;
            case 5:
                intervals.add(new Interval(0, 300));
                intervals.add(new Interval(800, 1100, Color.RED));
                intervals.add(new Interval(7250, 7500));
                intervals.add(new Interval(9000, 9300));
                intervals.add(new Interval(9300, 9410, Color.RED));
                intervals.add(new Interval(9410, 9510, Color.BLUE));
                intervals.add(new Interval(9510, 9610, Color.RED));
                intervals.add(new Interval(9610, 10000, Color.BLUE));
                intervals.add(new Interval(10100, 10500, Color.CYAN));
                break;
            case 6:

                intervals.add(new Interval(2200, 2700));
                intervals.add(new Interval(2700, 2730, Color.RED));
                intervals.add(new Interval(2830, 3000));
                break;
            case 7:
                intervals.add(new Interval(400, 700, Color.BLUE));
                intervals.add(new Interval(1500, 2000));
                intervals.add(new Interval(2000, 2200, Color.RED));
                intervals.add(new Interval(2700, 2730, Color.RED));
                intervals.add(new Interval(2730, 2800, Color.BLUE));
                intervals.add(new Interval(2800, 3000));
                intervals.add(new Interval(7650, 7800, Color.BLUE));
                intervals.add(new Interval(8650, 9000));
                break;
            case 8:

                intervals.add(new Interval(1150, 1400));
                intervals.add(new Interval(1400, 1600, Color.RED));
                intervals.add(new Interval(1700, 1900, Color.BLUE));
                intervals.add(new Interval(2800, 3000));
                intervals.add(new Interval(7900, 8100, Color.BLUE));
                break;
            case 9:
                intervals.add(new Interval(800, 1000));
                intervals.add(new Interval(1000, 1100, Color.RED));
                intervals.add(new Interval(8200, 8250, Color.RED));
                intervals.add(new Interval(8250, 8500, Color.BLUE));
                break;
            }
        } else if (board == 9) {
            switch (number) {
            // Brett 9

            case 0:
                intervals.add(new Interval(2600, 2700));
                intervals.add(new Interval(2700, 2800, Color.RED));
                intervals.add(new Interval(2800, 2900, Color.BLUE));

                intervals.add(new Interval(3000, 3200, Color.BLUE));
                intervals.add(new Interval(3200, 3300, Color.RED));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(4400, 4800, Color.BLUE));
                intervals.add(new Interval(4800, 5200, Color.RED));
                intervals.add(new Interval(5200, 5600, Color.BLUE));
                intervals.add(new Interval(5600, 6000, Color.RED));
                intervals.add(new Interval(9500, 9700));
                break;
            case 1:
                intervals.add(new Interval(2600, 2700));
                intervals.add(new Interval(2700, 2800, Color.RED));
                intervals.add(new Interval(2800, 2900, Color.BLUE));

                intervals.add(new Interval(3000, 3200, Color.BLUE));
                intervals.add(new Interval(3200, 3300, Color.RED));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(4400, 4800, Color.BLUE));
                intervals.add(new Interval(4800, 5200, Color.RED));
                intervals.add(new Interval(5200, 5600, Color.BLUE));
                intervals.add(new Interval(5600, 6000, Color.RED));
                intervals.add(new Interval(9100, 9400));
                break;
            case 2:
                intervals.add(new Interval(1700, 1900, Color.BLUE));
                intervals.add(new Interval(1900, 2100, Color.RED));
                intervals.add(new Interval(2100, 2500));

                intervals.add(new Interval(2600, 2700, Color.BLUE));
                intervals.add(new Interval(2700, 2800, Color.RED));
                intervals.add(new Interval(2800, 2900, Color.BLUE));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(8900, 9100));
                intervals.add(new Interval(9800, 10200, Color.CYAN));
                break;
            case 3:
                intervals.add(new Interval(1700, 1900, Color.BLUE));
                intervals.add(new Interval(1900, 2100, Color.RED));
                intervals.add(new Interval(2100, 2500));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(4300, 4400));
                intervals.add(new Interval(4400, 4800, Color.RED));
                intervals.add(new Interval(4800, 5200, Color.BLUE));
                intervals.add(new Interval(5200, 5600, Color.RED));
                intervals.add(new Interval(5600, 6000, Color.BLUE));

                intervals.add(new Interval(6100, 6300));
                intervals.add(new Interval(6600, 6800));
                intervals.add(new Interval(7200, 7400));
                intervals.add(new Interval(7800, 8000, Color.RED));
                intervals.add(new Interval(8400, 8600, Color.BLUE));
                intervals.add(new Interval(8600, 8900));

                intervals.add(new Interval(9800, 10200, Color.CYAN));
                break;
            case 4:
                intervals.add(new Interval(000, 400));
                intervals.add(new Interval(800, 1000, Color.BLUE));
                intervals.add(new Interval(1000, 1200, Color.RED));
                intervals.add(new Interval(1200, 1600));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400));
                intervals.add(new Interval(4400, 4800, Color.RED));
                intervals.add(new Interval(4800, 5200, Color.BLUE));
                intervals.add(new Interval(5200, 5600, Color.RED));
                intervals.add(new Interval(5600, 6000, Color.BLUE));
                intervals.add(new Interval(6100, 6500));
                intervals.add(new Interval(6900, 7100));
                intervals.add(new Interval(7500, 7700));
                intervals.add(new Interval(8100, 8300));
                break;
            case 5:
                intervals.add(new Interval(000, 400));

                intervals.add(new Interval(800, 1000, Color.BLUE));
                intervals.add(new Interval(1000, 1200, Color.RED));
                intervals.add(new Interval(1200, 1600));

                intervals.add(new Interval(2700, 2780, Color.BLUE));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400));
                intervals.add(new Interval(4400, 4800, Color.RED));
                intervals.add(new Interval(4800, 5200, Color.BLUE));
                intervals.add(new Interval(5200, 5600, Color.RED));
                intervals.add(new Interval(5600, 6000, Color.BLUE));
                intervals.add(new Interval(6100, 6500));
                intervals.add(new Interval(6900, 7100));
                intervals.add(new Interval(7500, 7700));
                intervals.add(new Interval(8100, 8300));
                break;
            case 6:
                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(4300, 4400));
                intervals.add(new Interval(4400, 4800, Color.RED));
                intervals.add(new Interval(4800, 5200, Color.BLUE));
                intervals.add(new Interval(5200, 5600, Color.RED));
                intervals.add(new Interval(5600, 6000, Color.BLUE));
                intervals.add(new Interval(6100, 6300));
                intervals.add(new Interval(6600, 6800));

                intervals.add(new Interval(7200, 7400));
                intervals.add(new Interval(7800, 8000, Color.BLUE));
                intervals.add(new Interval(8400, 8600, Color.RED));
                break;
            case 7:
                intervals.add(new Interval(400, 550));
                intervals.add(new Interval(550, 650, Color.RED));
                intervals.add(new Interval(650, 800));
                intervals.add(new Interval(2780, 2860, Color.BLUE));

                intervals.add(new Interval(3400, 3600));

                intervals.add(new Interval(3800, 4400, Color.RED));
                break;
            case 8:
                intervals.add(new Interval(400, 550));
                intervals.add(new Interval(550, 650, Color.RED));
                intervals.add(new Interval(650, 800));

                intervals.add(new Interval(3400, 3600, Color.BLUE));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(4400, 4800, Color.BLUE));
                intervals.add(new Interval(4800, 5200, Color.RED));
                intervals.add(new Interval(5200, 5600, Color.BLUE));
                intervals.add(new Interval(5600, 6000, Color.RED));
                break;
            case 9:
                intervals.add(new Interval(2940, 3200, Color.CYAN));

                intervals.add(new Interval(3400, 3600, Color.BLUE));

                intervals.add(new Interval(3800, 4400, Color.RED));
                intervals.add(new Interval(4400, 4800, Color.BLUE));
                intervals.add(new Interval(4800, 5200, Color.RED));
                intervals.add(new Interval(5200, 5600, Color.BLUE));
                intervals.add(new Interval(5600, 6000, Color.RED));
                break;
            }
        } else if (board == 10) {
            switch (number) {
            // Brett 10

            case 0:
                intervals.add(new Interval(600, 900));
                intervals.add(new Interval(1030, 2300));
                intervals.add(new Interval(2400, 2600));
                intervals.add(new Interval(2700, 2900));
                intervals.add(new Interval(3000, 3600));
                intervals.add(new Interval(3700, 4100));
                intervals.add(new Interval(4200, 4700));
                intervals.add(new Interval(4800, 5500));
                intervals.add(new Interval(5600, 6500));
                intervals.add(new Interval(6600, 7600));
                intervals.add(new Interval(7700, 8800));
                intervals.add(new Interval(8900, 9200));
                intervals.add(new Interval(9300, 9500));
                break;
            case 1:
                intervals.add(new Interval(600, 933));
                intervals.add(new Interval(1067, 2200, Color.BLUE));
                intervals.add(new Interval(3400, 3800));
                intervals.add(new Interval(8900, 9200));
                intervals.add(new Interval(9600, 9900));
                break;
            case 2:
                intervals.add(new Interval(600, 967, Color.BLUE));
                intervals.add(new Interval(1100, 2100));
                intervals.add(new Interval(3600, 4100));
                intervals.add(new Interval(8500, 8800));
                intervals.add(new Interval(10000, 10300));
                break;
            case 3:
                intervals.add(new Interval(600, 1000));
                intervals.add(new Interval(1130, 2000));
                intervals.add(new Interval(3900, 4400));
                intervals.add(new Interval(8100, 8400));
                intervals.add(new Interval(10400, 10700));
                break;
            case 4:
                intervals.add(new Interval(000, 600));
                intervals.add(new Interval(1150, 1500));
                intervals.add(new Interval(4200, 4600));
                intervals.add(new Interval(7700, 8000));
                intervals.add(new Interval(10800, 11000));
                intervals.add(new Interval(11000, 11300, Color.CYAN));
                break;
            case 5:
                intervals.add(new Interval(0, 600));
                intervals.add(new Interval(1500, 1800));
                intervals.add(new Interval(4400, 5000));
                intervals.add(new Interval(7300, 7600));
                intervals.add(new Interval(10800, 11000));
                intervals.add(new Interval(11000, 11300, Color.CYAN));
                break;
            case 6:
                intervals.add(new Interval(600, 1000));
                intervals.add(new Interval(1130, 2000));
                intervals.add(new Interval(4800, 5200));
                intervals.add(new Interval(6900, 7200));
                break;
            case 7:
                intervals.add(new Interval(600, 967, Color.BLUE));
                intervals.add(new Interval(1100, 2100));
                intervals.add(new Interval(5000, 5500));
                intervals.add(new Interval(6500, 6800));
                break;
            case 8:
                intervals.add(new Interval(600, 933));
                intervals.add(new Interval(1067, 2200, Color.BLUE));
                intervals.add(new Interval(5300, 5800));
                intervals.add(new Interval(6100, 6400));
                break;
            case 9:
                intervals.add(new Interval(1030, 2600));
                intervals.add(new Interval(2700, 2900, Color.RED));
                intervals.add(new Interval(5600, 6000, Color.BLUE));
                break;
            }
        }
    }

    public Interval getInterval(int index) {
        return intervals.get(index);
    }

    public List<String> getName() {
        return name;
    }

    public int getNumberOfIntervals() {
        return intervals.size();
    }
}