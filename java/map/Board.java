package map;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

/**
 * Board is initialized by the Game object, and creates 10 Pillar objects.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class Board {

    private boolean available;
    private int boardNumber;
    private Image image;
    private Image lockedImage;
    private List<Pillar> pillars = new ArrayList<Pillar>();

    public Board(int x, boolean available) {
        this.available = available;
        boardNumber = x;
        image = new ImageIcon(Board.class.getResource("level" + x + ".jpg"))
                .getImage();
        if (x > 1) {
            lockedImage = new ImageIcon(Board.class.getResource("level" + x
                    + "locked.jpg")).getImage();
        }
        for (int i = 0; i < 10; i++) {
            pillars.add(new Pillar(i, boardNumber));
        }
    }

    public int getBoardNumber() {
        return boardNumber;
    }

    public Image getImage() {
        return image;
    }

    public Image getLockedImage() {
        return lockedImage;
    }

    public int getNumberOfPillars() {
        return pillars.size();
    }

    public Pillar getPillar(int i) {
        return pillars.get(i);
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
