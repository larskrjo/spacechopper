package map;

import java.awt.Color;

/**
 * Interval is initialized by a Pillar object, and it holds on a start and end
 * position and a Color.
 * 
 * @version 1.000, 01/05/09
 * @author H�vard L�demel, Lars Kristian Johansen, Sigurd Wien, H�vard Malm
 *         Geithus
 * 
 */
public class Interval {

    private Color color;
    private int end;
    private int start;

    public Interval(int start, int end) {
        this(start, end, new Color(114, 202, 177));
    }

    public Interval(int start, int end, Color color) {
        this.start = start;
        this.end = end;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public int getEnd() {
        return end;
    }

    public int getStart() {
        return start;
    }
}
